# ------------------------------------------------------------------------------------------------- 
# Copyright (c) 2018, IIT Madras All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
# 
# - Redistributions of source code must retain the below copyright notice, this list of conditions
#   and the following disclaimer.  
# - Redistributions in binary form must reproduce the above copyright notice, this list of 
#   conditions and the following disclaimer in the documentation and/or other materials provided 
#   with the distribution.  
# - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
#   promote products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# -------------------------------------------------------------------------------------------------
# Author: Lavanya J
# Email id: lavanya.jagan@gmail.com
# -------------------------------------------------------------------------------------------------
#
#!/usr/bin/perl

#-----------------------------------------------------------
# makeTest.pl
# Generates test related files
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use POSIX ":sys_wait_h";
use testRunConfig;
use scriptUtils;

checkSetup();

# Parse options
GetOptions(
          qw(isa=s)  => \my $isa,
          qw(mk_opts) => \my $mk_opts,
          qw(help)    => \my $help,
          qw(clean)   => \my $clean
);

if (!$isa) {
  doPrint("ERROR: Undefined ISA\n");
  exit(1);
}
testRunConfig::setConfigValue("CONFIG_LOG",1);

doDebugPrint("Compiling desgin with ISA=$isa\n");
if ($isa =~ /RV64/) {
  systemCmd("make -j16 restore");
  chdir("$shaktiHome/verification/riscv-tests/env");
  `git apply --check $shaktiHome/verification/patches/riscv-tests-shakti-signature.patch`;
  my $patch_check = $?;
  if ($patch_check == 0) {
    systemCmd("git apply $shaktiHome/verification/patches/riscv-tests-shakti-signature.patch");
  }
  chdir($shaktiHome);
  `make -j16 generate_verilog SYNTH=SIM MUL=fpga ISA=$isa MULSTAGES=8 DIVSTAGES=64 VERBOSITY=0 $mk_opts`;
  systemCmd("make -j16 link_verilator generate_boot_files");
}
elsif ($isa =~ /RV32/) {
  systemCmd("make -j16 restore");
  chdir("$shaktiHome/verification/riscv-tests/env");
  `git apply --check $shaktiHome/verification/patches/riscv-tests-shakti-signature.patch`;
  my $patch_check = $?;
  if ($patch_check == 0) {
    systemCmd("git apply $shaktiHome/verification/patches/riscv-tests-shakti-signature.patch");
  }
  chdir($shaktiHome);
  `make -j16 generate_verilog SYNTH=SIM MUL=fpga ISA=$isa MULSTAGES=4 DIVSTAGES=32 VERBOSITY=0 $mk_opts`;
  systemCmd("make -j16 link_verilator generate_boot_files");
}
else {
  doPrint("ERROR: Invalid ISA\n");
  exit(1);
}

closeLog();
appendLog("$workdir/$scriptLog.log");
