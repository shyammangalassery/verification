#!/usr/bin/perl

#-----------------------------------------------------------
# makeRegress.pl
# Generates test related files
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use testRunConfig; 
use scriptUtils;
use List::Util;
use POSIX qw{setsid};
checkSetup();
setEnvConfig();

my $simulator = getConfig("CONFIG_SIM");


# Parse options
GetOptions(
          qw(submit)        => \my $submit_tests,
          qw(compile)       => \my $src_compile,
          qw(generate)      => \my $generate_tests,
          qw(final)         => \my $final_report,
          qw(list=s)        => \my $test_list,
          qw(suite=s)       => \my $test_suite,
          qw(test_opts=s)   => \my $test_opts,
          qw(filter=s)      => \my $test_filter,
          qw(result=s)      => \my $result_filter,
          qw(test_count=s)  => \my $test_count,
          qw(parallel=s)      => \my $parallel,
          qw(report)        => \my $report,
          qw(debug)          => \my $debug,
          qw(help)          => \my $help,
          qw(clean)         => \my $clean
);

my $submit;
my $testSuite;
my $testCount;
my $generate;
my $testList;
my $filter;
my $compile;
my $finalReport;

# test submission if not given, we simply report
if ($submit_tests) {
  $submit = 1;
}
else {
  $submit = 0;
}
if (defined $ENV{'CONFIG_COV'}) {
    setConfigValue("CONFIG_COV", $ENV{'CONFIG_COV'});
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

if ($final_report) {
  $finalReport = 1;
}
else {
  $finalReport = 0;
}

if (!$parallel) {
  $parallel=0;
}
else {
  setConfigValue("CONFIG_LOG", 0);
}
if ($src_compile) {
  $compile = 1;
}
else {
  $compile = 0;
}

if (!$test_list) {
  $testList = "$scriptPath/core_configs/$coreClass-riscv-tests.list";
}
else {
  if($test_list =~ /benchmark/) {
    $testList = "$scriptPath/core_configs/$coreClass-benchmark-tests.list";
  }
  else {
    $testList = "$workdir/$test_list";
  }
}

if ($generate_tests) {
  $generate = 1;
}
else {
  $generate = 0;
}

if (!$test_filter) {
  $filter = "";
}
else {
  $filter = $test_filter;
}

# Test suite
if ($test_suite) {
  $testSuite = $test_suite;
  if ($testSuite =~ /riscv-tests/) {
    $testList = "$scriptPath/core_configs/$coreClass-riscv-tests.list";
  }
  elsif ($testSuite =~ /aapg/) {
    $testList = "$workdir/aapg.list";
    unless (-e $testList) {
      die "ERROR: No aapg tests have been generated\n";
    }
  }
}
else {
  $testSuite = "all"; 
}

# Test count
if ($test_count) {
  $testCount = $test_count;
}
else {
  my $count = $ENV{'CONFIG_GEN_COUNT'};
  if ($count) {
    $testCount = $count;
  }
  else {
    $testCount = 1;
  }
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}
else {
  checkBins();
}
my $MaxCount = 0;
my $count = 0;
if ($generate) {
  chdir("$scriptPath");
  systemCmd("rm -rf $shaktiHome/verification/tests/random/riscv-torture/generated_tests/*");
  systemCmd("rm -rf $shaktiHome/verification/tests/random/aapg/generated_tests/*");
  if ($testSuite =~ /^all$/ || $testSuite=~ /^aapg$/) {
    my @configs = `ls $shaktiHome/verification/tests/random/aapg/configs/*.py`;
    $MaxCount = $MaxCount + scalar(@configs);
    systemCmd("perl makeAapg.pl --config=all --test_count=$testCount&");
  }
  if ($testSuite =~ /^all$/ || $testSuite=~ /^riscv-torture$/) {
    my @configs = `ls $shaktiHome/verification/tests/random/riscv-torture/configs/*.config`;
    $MaxCount = $MaxCount + scalar(@configs);
    systemCmd("perl makeTorture.pl --config=all --test_count=$testCount");
  }
  $MaxCount = $MaxCount * $testCount;
  $count = `find $shaktiHome/verification/tests/random/ -name "*.S" | wc -l`;
  doPrint("Test count to be generated = $MaxCount | Current count = $count\n");
  if (($testSuite =~ /^all$/) || ($testSuite=~ /^riscv-torture$/) || ($testSuite =~ /^aapg$/)) {
    my $timeout = 0;
    while ($count != $MaxCount) {
      sleep(10);
      $count = `find $shaktiHome/verification/tests/random/ -name "*.S" | wc -l`;
      chomp($count);
      doDebugPrint("Current test count = $count\n");
      $timeout++;
      if ($timeout == 300) {
        last;
      }
    }
  }
  open TESTLIST, "$scriptPath/riscv-tests.list" or die "[$scriptLog.pl] ERROR opening file $!\n";
  my @listFile = <TESTLIST>;
  close TESTLIST;
  my @genTests = `find  $shaktiHome/verification/tests/random/riscv-torture -name "*.S"`;
  chomp(@genTests);
  foreach my $test (@genTests) {
    my $file = `basename $test .S`; chomp($file);
    my $suite = `dirname $test`; chomp($suite);
    $suite = substr($suite, index($suite, "random/"));
    push @listFile, "$file\t\t\t\t$suite\t\t\t\tp\n";
  }
  open TESTLIST, ">$scriptPath/tests.list" or die "[$scriptLog.pl] ERROR opening file $!\n";
  print TESTLIST @listFile;
  close TESTLIST;
}
####### src compilation
if ($compile) {
  if ($simulator =~ /^bluespec$/) {
    chdir($shaktiHome);
    doDebugPrint("cd $shaktiHome\n");
    systemCmd("make restore");
    systemCmd("make compile_bluesim");
    systemCmd("make link_bluesim");
    systemCmd("make generate_boot_files");
  }
  elsif ($simulator =~ /^ncverilog$/) {
    chdir($shaktiHome);
    doDebugPrint("cd $shaktiHome\n");
    systemCmd("make restore");
    systemCmd("make generate_verilog ");
    systemCmd("make compile_ncverilog");
    systemCmd("make link_ncverilog");
    systemCmd("make generate_boot_files");
  }
}

#######
# Process tests.list -----------
open TESTLIST, "$testList" or die "[$scriptLog.pl] ERROR opening file $testList\n";
my @listFile = <TESTLIST>;
close TESTLIST;
my @testList = ();

foreach my $line (@listFile) { # remove unwanted white space
  if ($line !~ /^\s*\#/) {
    if ($line !~ /^\s*$/) {
      my @line = split(" ", $line);
      chomp(@line);
      $line = join(" ", @line);
      push @testList, $line;
    }
  }
}

if ($filter) {
  @testList = grep /$filter/, @testList;
}
# run the tests 
if ($submit) {
  my $totalTests = scalar @testList;
  my $licenseCount = $parallel;
  my @tempTests = @testList;
  my $count = 0;
  my $refreshCount = 0;

  while (@tempTests) {
    if (($parallel > 0) && ($parallel < @tempTests)) {
      for (1 .. $parallel) {
        my $line = shift @tempTests;
        my @line = split(" ", $line);
        my $test = $line[0];
        my $tSuite = $line[1];
        my $pv = $line[2];
        unless (-e $workdir or mkdir -p $workdir/$tSuite) {
          die "ERROR: Unable to create workdir, check SHAKTI_HOME is set!\n";
        }
        my $pid;
        $pid = fork();
        die if not defined $pid;
        if (not $pid) {
          my $ret = systemCmd("perl -I $scriptPath $scriptPath/makeTest.pl --test=$test --suite=$tSuite --type=$pv $test_opts");
          setpgrp(0,0);
          chdir('/');
          exit($ret);
        }
      }
      1 while (wait() != -1);
    }
    elsif ($parallel >= @tempTests) {
      for (1 .. @tempTests) {
        my $line = shift @tempTests;
        my @line = split(" ", $line);
        my $test = $line[0];
        my $tSuite = $line[1];
        my $pv = $line[2];

        my $pid;
        $pid = fork();
        die if not defined $pid;
        if (not $pid) {
          my $ret = systemCmd("perl -I $scriptPath $scriptPath/makeTest.pl --test=$test --suite=$tSuite --type=$pv $test_opts");
          setpgrp(0,0);
          chdir('/');
          exit($ret);
        }
      }
    }
    else {
      my $line = shift @tempTests;
      my @line = split(" ", $line);
      my $test = $line[0];
      my $tSuite = $line[1];
      my $pv = $line[2];
      unless (-e $workdir or mkdir -p $workdir/$tSuite) {
        die "ERROR: Unable to create workdir, check SHAKTI_HOME is set!\n";
      }
      # using system instead of systemCmd to print the result while running and not the makeTest command
      system("perl -I $scriptPath $scriptPath/makeTest.pl --test=$test --suite=$tSuite --type=$pv $test_opts");
    }
  }
  if ($test_list =~ /benchmark/) {
      system("perl -I $scriptPath $scriptPath/makeBenchmark.pl");
  }
}
elsif ($finalReport) { # waits till all the test results are there/timesout
  my %testResults = ();
  my @regress_report = ();
  my $timeout = 0; 
  my $passCount = 0;

  foreach my $line (@testList) {
    $testResults{$line} = "NOT_RUN";
  }
  while (keys %testResults) {
    foreach my $line (keys %testResults) {
      my @line = split(" ", $line);
      my $test = $line[0];
      my $tSuite = $line[1];
      my $pv = $line[2];
      if ($tSuite =~ /compliance\*(.*)/) {
        $tSuite=$1;
      }
      my $pass = "$workdir/$tSuite/$pv/$test/PASSED";
      my $no_result = "$workdir/$tSuite/$pv/$test/NO_RESULT";
      my $fail = "$workdir/$tSuite/$pv/$test/FAILED";
      my $compile_fail = "$workdir/$tSuite/$pv/$test/FAIL_COMPILE";
      my $model_fail = "$workdir/$tSuite/$pv/$test/FAIL_MODEL";
      my $rtl_fail = "$workdir/$tSuite/$pv/$test/FAIL_RTL";
      my $sign_fail = "$workdir/$tSuite/$pv/$test/FAIL_SIGN";
      my $rtl_timeout = "$workdir/$tSuite/$pv/$test/FAIL_RTL_TIMEOUT";
      my $result;
      my $delete=0;

      if (-e $compile_fail) {
        $result = sprintf("%40s %40s %5s    FAIL_COMPILE\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $model_fail) {
        $result = sprintf("%40s %40s %5s    FAIL_MODEL\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $rtl_fail) {
        $result = sprintf("%40s %40s %5s    FAIL_RTL\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $sign_fail) {
        $result = sprintf("%40s %40s %5s    FAIL_SIGN\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $rtl_timeout) {
        $result = sprintf("%40s %40s %5s    FAIL_RTL_TIMEOUT\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $fail) {
        $result = sprintf("%40s %40s %5s    FAILED\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $no_result) {
        $result = sprintf("%40s %40s %5s    NO_RESULT\n", $test, $tSuite, $pv);
        $delete = 1;
      }
      elsif (-e $pass) {
        $result = sprintf("%40s %40s %5s    PASSED\n", $test, $tSuite, $pv);
        $delete = 1;
        $passCount++;
      }
      else {
        $result = sprintf("%40s %40s %5s    NOT_RUN\n", $test, $tSuite, $pv);
        $delete = 0;
      }
      if ($delete) {
        push @regress_report, $result;
        delete $testResults{$line};
      }
    }# end of foreach
    if (keys %testResults) {
      sleep(5);
    }
    $timeout++;
    if ($timeout == 300) {
      last;
    }
  } # end of while
  if (keys %testResults) {
    print @regress_report;
    # TODO:print NOT_RUN tests
  }
  else {
    print @regress_report;
    if ($passCount == @testList) {
      exit(0);
      `touch $workdir/REGRESS_PASS`;
    }
    else {
      exit(100);
      `touch $workdir/REGRESS_FAIL`;
    }
  }
  if ($ENV{'CONFIG_COV'} == 1) {
      systemCmd("xmlcoverage merge --directory=$workdir --out_file=$workdir/cov_merged.xml ");
  }
}
if ($submit == 0) {
  my @regress_report = ();
  my %result = ();
  foreach my $line (@testList) {
    my @line = split(" ", $line);
    my $test = $line[0];
    my $tSuite = $line[1];
    if ($tSuite =~ /compliance\*(.*)/) {
      $tSuite=$1;
    }
    my $pv = $line[2];
    my $pass = "$workdir/$tSuite/$pv/$test/PASSED";
    my $no_result = "$workdir/$tSuite/$pv/$test/NO_RESULT";
    my $fail = "$workdir/$tSuite/$pv/$test/FAILED";
    my $compile_fail = "$workdir/$tSuite/$pv/$test/FAIL_COMPILE";
    my $model_fail = "$workdir/$tSuite/$pv/$test/FAIL_MODEL";
    my $rtl_fail = "$workdir/$tSuite/$pv/$test/FAIL_RTL";
    my $sign_fail = "$workdir/$tSuite/$pv/$test/FAIL_SIGN";
    my $rtl_timeout = "$workdir/$tSuite/$pv/$test/FAIL_RTL_TIMEOUT";
    my $result;

    my $test_info=sprintf("%40s %40s %5s", $test, $tSuite, $pv);
    if (-e $compile_fail) {
      $result = sprintf("%40s %40s %5s    FAIL_COMPILE\n", $test, $tSuite, $pv);
      $result{$test_info}="FAIL_COMPILE";
    }
    elsif (-e $model_fail) {
      $result = sprintf("%40s %40s %5s    FAIL_MODEL\n", $test, $tSuite, $pv);
      $result{$test_info}="FAIL_MODEL";
    }
    elsif (-e $rtl_fail) {
      $result = sprintf("%40s %40s %5s    FAIL_RTL\n", $test, $tSuite, $pv);
      $result{$test_info}="FAIL_RTL";
    }
    elsif (-e $sign_fail) {
      $result = sprintf("%40s %40s %5s    FAIL_SIGN\n", $test, $tSuite, $pv);
      $result{$test_info}="FAIL_SIGN";
    }
    elsif (-e $rtl_timeout) {
      $result = sprintf("%40s %40s %5s    FAIL_RTL_TIMEOUT\n", $test, $tSuite, $pv);
      $result{$test_info}="FAIL_RTL_TIMEOUT";
    }
    elsif (-e $fail) {
      $result = sprintf("%40s %40s %5s    FAILED\n", $test, $tSuite, $pv);
      $result{$test_info}="FAILED";
    }
    elsif (-e $no_result) {
      $result = sprintf("%40s %40s %5s    NO_RESULT\n", $test, $tSuite, $pv);
      $result{$test_info}="NO_RESULT";
    }
    elsif (-e $pass) {
      $result = sprintf("%40s %40s %5s    PASSED\n", $test, $tSuite, $pv);
      $result{$test_info}="PASSED";
    }
    else {
      $result = sprintf("%40s %40s %5s    NOT_RUN\n", $test, $tSuite, $pv);
      $result{$test_info}="NOT_RUN";
    }
    push @regress_report, $result;
  }
  if ($result_filter) {
    @regress_report = grep /$result_filter/i, @regress_report;
  }
  #print @regress_report;
  my @resultValues = values %result;
  open REPORT, ">$shaktiHome/verification/workdir/regress_report.log" or die "[$scriptLog.pl] ERROR opening file $!\n";
  print REPORT "#------------------------------------------------------------------------------------------------\n";
  print REPORT "# Total tests      :", scalar(@regress_report),"\n";
  print REPORT "# Pass percentage  :", sprintf("%.2f\%", (scalar(grep /PASSED/, @regress_report)/scalar(@regress_report))*100),"\n";
  print REPORT "# FAILED           :", scalar(grep /FAILED/, @regress_report),"\n";
  print REPORT "# FAIL_RTL         :", scalar(grep /FAIL_RTL/, @regress_report),"\n";
  print REPORT "# FAIL_SIGN        :", scalar(grep /FAIL_SIGN/, @regress_report),"\n";
  print REPORT "# FAIL_RTL_TIMEOUT :", scalar(grep /FAIL_RTL_TIMEOUT/, @regress_report),"\n";
  print REPORT "# FAIL_COMPILE     :", scalar(grep /FAIL_COMPILE/, @regress_report),"\n";
  print REPORT "# FAIL_MODEL       :", scalar(grep /FAIL_MODEL/, @regress_report),"\n";
  print REPORT "# NOT_RUN          :", scalar(grep /NOT_RUN/, @regress_report),"\n";
  print REPORT "# PASSED           :", scalar(grep /PASSED/, @regress_report),"\n";
  print REPORT "#------------------------------------------------------------------------------------------------\n";
  print REPORT "$_     $result{$_}\n" foreach(sort{$result{$a} cmp $result{$b}} keys %result);
  close REPORT;
  print "$_     $result{$_}\n" foreach(sort{$result{$a} cmp $result{$b}} keys %result);
  print  "#------------------------------------------------------------------------------------------------\n";
  print "# Pass percentage  :", sprintf("%.2f\%", (scalar(grep /PASSED/, @regress_report)/scalar(@regress_report))*100),"\n";
  print  "#------------------------------------------------------------------------------------------------\n";
}
