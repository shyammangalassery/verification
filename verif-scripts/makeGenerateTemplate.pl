# ------------------------------------------------------------------------------------------------- 
# Copyright (c) 2018, IIT Madras All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without modification, are permitted
# provided that the following conditions are met:
# 
# - Redistributions of source code must retain the below copyright notice, this list of conditions
#   and the following disclaimer.  
# - Redistributions in binary form must reproduce the above copyright notice, this list of 
#   conditions and the following disclaimer in the documentation and/or other materials provided 
#   with the distribution.  
# - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
#   promote products derived from this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# -------------------------------------------------------------------------------------------------
# Author: Lavanya J
# Email id: lavanya.jagan@gmail.com
# -------------------------------------------------------------------------------------------------
#
#!/usr/bin/perl

#-----------------------------------------------------------
# Generates template file for AAPG
#-----------------------------------------------------------

#illegal decode 
#instruction mis-aligned execute
#ld/st misaligned execute
#
#load access: memory
#store acces: write back
#
#page fault: memory
#ld/st/instruc

use strict;
use Getopt::Long;
use testRunConfig;
use scriptUtils;
use excepConfig;

checkSetup();
setEnvConfig();
clearCountAll();

my $simulator = getConfig("CONFIG_SIM");
my $configPath = "$shaktiHome/verification/verif-scripts/core_configs";
my $testPath =  "$workdir/aapg";
my $output_dir;
my $filename;
# todo: make all lw to LREG, sw to SREG etc

# Parse options
GetOptions(
          qw(noexcep_macros)  => \my $no_excep,
          qw(nobranch_macros) => \my $no_branch,
          qw(out_dir=s)       => \my $out_dir,
          qw(name=s)          => \my $name,
          qw(test=s)          => \my $test,
          qw(type=s)          => \my $test_type,
          qw(debug)           => \my $debug,
          qw(help)            => \my $help,
          qw(clean)           => \my $clean
);

my $testType;
testRunConfig::setEnvConfig();
if (!$test) {
  doPrint("ERROR: Test file not found\n");
  exit(1);
}
if (!$out_dir) {
  $output_dir="$ENV{'PWD'}/generated_templates";
}
else {
  $output_dir = $out_dir;
}
if ($test_type) {
  $testType = $test_type;
}
else {
  $testType = "p";
}

if (!$name) {
  $filename = "templates";
}
else {
  $filename = $name;
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", 1);
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}



my %exception_template = ();
$exception_template{'ecause00'} = '# Instruction address mis-aligned';
$exception_template{'ecause01'} = '# Instruction access fault';
$exception_template{'ecause02'} = '# Illegal instruction';
$exception_template{'ecause03'} = '# Breakpoint';
$exception_template{'ecause04'} = '# Load address misaligned';
$exception_template{'ecause05'} = '# Load access fault';
$exception_template{'ecause06'} = '# Store/AMO address misaligned';
$exception_template{'ecause07'} = '# Store/AMO access fault ';
$exception_template{'ecause08'} = '# Env call from U-mode';
$exception_template{'ecause09'} = '# Env call from S-mode';
$exception_template{'ecause10'} = '# Reserved';
$exception_template{'ecause11'} = '# Env call from M-mode';
$exception_template{'ecause12'} = '# Instruction page fault';
$exception_template{'ecause13'} = '# Load page fault';
$exception_template{'ecause14'} = '# Reserved';
$exception_template{'ecause15'} = '# Store/AMO page fault';

my $testName = `basename -s .S $test`;chomp($testName);
if (-e "$test") {
  open INFILE, "$test";
  open OUTFILE, ">$workdir/aapg/$testType/$testName/$testName\_dup.S";
}
else {
  doPrint("ERROR: Can't open file $testName\_dup.S\n");
  exit(1);
}
while (<INFILE>) {
  my $line = $_;
  if ($line =~ /(i.*:\s*)(ecause\d+)\s*/) {
    my $key = $2;
    my $excep = $1 . $key;
    if (($key !~ /ecause03/) && ($key !~ /ecause08/) && ($key !~ /ecause09/) && ($key !~ /ecause11/) ) {
      $excepCount{$key}++;
      s/$excep/$excep\_$excepCount{$key}/g;
      print OUTFILE $_;
    }
    else {
      print OUTFILE $line;
    }
  }
  elsif ($line =~ /(i.*:.*)ecause00\s*/) {
    my $excep = $1 . 'ecause00';
    $excepCount{'ecause00'}++;
    s/$excep/$excep\_$excepCount{'ecause00'}/g;
    print OUTFILE $_;
  }
  else {
    print OUTFILE $line;
  }
}

print OUTFILE ".align 4; .global end_signature; end_signature:\n";
#printExcepCount();

sub random_ecause {
  my $key = (keys %excepCount)[rand keys %excepCount];
  if ($excepCount{$key} > 0) {
    if (($key !~ /ecause03/) && ($key !~ /ecause08/) && ($key !~ /ecause09/) && ($key !~ /ecause11/) ) {
      my $random_value = int(rand(($excepCount{(keys %excepCount)[rand keys %excepCount]}) -1)) + 1;
      return "$key\_$random_value";
    }
    else {
      return $key;
    }
  }
  else {
    return "";
  }
}


if (-e "$output_dir/$filename\.S") {
  doPrint(" Using $output_dir/$filename\.S\n");
}
else {
  doPrint(" Generating $output_dir/$filename\.S\n");

  `mkdir -p $output_dir`;
  my @template;
  open TEMPLATE, ">$output_dir/$filename\.S" or die "ERROR opening file $! \n";
  my $index = 0; 
  my $reg1;
  my $reg2;
  # ------------------------------------
  # ecause00 # Instruction address mis-aligned
  # ------------------------------------
  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause00 = (
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG a1,			1*REGBYTES(sp)
    SREG x$reg1,  2*REGBYTES(sp)
  
    # exception
    la x$reg1, 2f
    la a1, 1f
  1:
    jr 2(x$reg1)
  2: 
    nop
    nop
    # stack pop
    LREG a1,		 1*REGBYTES(sp)
    LREG x$reg1, 2*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "  
    # stack push
    addi sp, sp, -3*REGBYTES
    SREG a1,		 1*REGBYTES(sp)
    SREG x$reg1, 2*REGBYTES(sp)
    SREG x$reg2, 3*REGBYTES(sp)
  
    # exception
    la x$reg1, 2f
    la a1, 1f
  1:
    jalr x$reg2, 2(x$reg1)
  2:
    nop
    nop
    # stack pop
    LREG a1,		 1*REGBYTES(sp)
    LREG x$reg1, 2*REGBYTES(sp)
    LREG x$reg2, 3*REGBYTES(sp)
    addi sp, sp, 3*REGBYTES
  
  "

  );
  
  # ------------------------------------
  # ecause01 # Instruction access fault
  # ------------------------------------
  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause01 = (
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG a1,			1*REGBYTES(sp)
  
    # exception
    la a1, 1f
  1:
    jr x0
    
    # stack pop
    LREG a1,			1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG a1,			1*REGBYTES(sp)
    SREG x$reg1,  2*REGBYTES(sp)
  
    # exception
    la a1, 1f
  1:
    jalr x$reg1, x0
  
    # stack pop
    LREG a1,			1*REGBYTES(sp)
    LREG x$reg1,  2*REGBYTES(sp)
    addi sp, sp,  2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG a1,			1*REGBYTES(sp)
    SREG x$reg1,  2*REGBYTES(sp)
  
    # exception
    la a1, 1f
    li x$reg1, 0
  1: 
    jr x$reg1
    
    # stack pop
    LREG a1,			1*REGBYTES(sp)
    LREG x$reg1,  2*REGBYTES(sp)
    addi sp, sp,  2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -3*REGBYTES
    SREG a1,			1*REGBYTES(sp)
    SREG x$reg1,  2*REGBYTES(sp)
    SREG x$reg2,  3*REGBYTES(sp)
  
    # exception
    la a1, 1f
    li x$reg1, 0
  1: 
    jalr x$reg2, x$reg1
    
    # stack pop
    LREG a1,			1*REGBYTES(sp)
    LREG x$reg1,  2*REGBYTES(sp)
    LREG x$reg2,  3*REGBYTES(sp)
    addi sp, sp,  3*REGBYTES
  
  ",
  );
  
  # ------------------------------------
  # ecause02 # Illegal instruction
  # ------------------------------------
  # generating illegal instruction list #
  my @illegal_instr_list = ();
  foreach my $count (0 .. 100) {
    my $d = int(rand(4294967295));
    my $hex = sprintf("0x%X", $d);
    my $illegal = `echo "DASM(0x$hex)" | spike-dasm`;
    if ($illegal =~ /un/ && $illegal !~ /counter/) {
      push @illegal_instr_list, ".word $hex";
    }
  }

  my @ecause02 = (
  "
    $illegal_instr_list[int(rand(@illegal_instr_list))] 
  "
  );
  
  # ------------------------------------
  # ecause03 # ebreak
  # ------------------------------------
  my @ecause03 = (
  "
    ebreak
  "
  );
  
  # ------------------------------------
  # ecause04 # load address misaligned
  # ------------------------------------
  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause04 = (
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREG x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREG x$reg2, 2(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREG x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREGU x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREGU x$reg2, 2(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    LREGU x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lw x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lw x$reg2, 2(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lw x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lh x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lh x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lhu x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    lhu x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREG x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREG x$reg2, 2(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREG x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREGU x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREGU x$reg2, 2(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREGU x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lw x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lw x$reg2, 2(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lw x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lh x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lh x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lhu x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    lhu x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREG x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREG x$reg2, 2(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREG x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREGU x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREGU x$reg2, 2(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    LREGU x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lw x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lw x$reg2, 2(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lw x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lh x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lh x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lhu x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    lhu x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  );
  # ------------------------------------
  # ecause05 # Load access fault
  # ------------------------------------
  # generating 12-bit unsigned addr list 
  my @load_addr_list = ();
  foreach my $count (0 .. 100) {
    my $d = int(rand(2000));
    push @load_addr_list, $d;
  }

  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause05 = (
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    LREG x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    LREG x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    LREG x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    LREGU x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    LREGU x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    LREGU x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lw x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    lw x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lw x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
# ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lh x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    lh x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lw x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lhu x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    lhu x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lhu x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lb x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    lb x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lb x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lbu x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    lbu x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    lbu x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  "
);
  # ------------------------------------
  # ecause06 # Store address misaligned
  # ------------------------------------
  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause06 = (
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    SREG x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    SREG x$reg2, 2(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    SREG x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    sw x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    sw x$reg2, 2(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    sw x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    sh x$reg2, 1(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg2,  1*REGBYTES(sp)
  
    #exception
    sh x$reg2, 3(sp)
    
    # stack pop
    LREG x$reg2, 1*REGBYTES(sp)
    addi sp, sp, 1*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    LREG x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    SREG x$reg2, 2(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    SREG x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    sw x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    sw x$reg2, 2(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    sw x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    sh x$reg2, 1(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    sh x$reg2, 3(x$reg1)
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",

  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    SREG x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    SREG x$reg2, 2(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    SREG x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    sw x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    sw x$reg2, 2(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    sw x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    sh x$reg2, 1(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  ",
  "
    # stack push
    addi sp, sp, -2*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
    SREG x$reg2,  2*REGBYTES(sp)
  
    #exception
    mv x$reg1, sp
    li x$reg2, 1
    mul x$reg1, x$reg1, x$reg2
    sh x$reg2, 3(x$reg1)
    mul x$reg1, x$reg1, x$reg2
    
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    LREG x$reg2,  2*REGBYTES(sp)
    addi sp, sp, 2*REGBYTES
  "
  );
  # ------------------------------------
  # ecause07 # Load access fault
  # ------------------------------------
  # generating 12-bit unsigned addr list 
  my @store_addr_list = ();
  foreach my $count (0 .. 100) {
    my $d = int(rand(2000));
    push @store_addr_list, $d;
  }
  $reg1 = int(rand(31)) + 1;
  $reg2 = int(rand(28));
  if ($reg1 == 11 || $reg1 == 2) {
    $reg1++;
  }
  if ($reg1 == $reg2) {
    $reg2++;
  }
  if ($reg2 == 11 || $reg2 == 2) {
    $reg2++;
  }
  my @ecause07 = (
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    SREG x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    SREG x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    SREG x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sw x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    sw x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sw x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
# ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sh x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    sh x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sh x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  # ------------------------------------
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sb x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  ",
  "
    # exception
    sb x0, (x0)
  ",
  "
    # stack push
    addi sp, sp, -1*REGBYTES
    SREG x$reg1,  1*REGBYTES(sp)
  
    # exception 
    li x$reg1, 0
    sb x$reg1, (x$reg1)
  
    # stack pop
    LREG x$reg1,  1*REGBYTES(sp)
    addi sp, sp,  1*REGBYTES
  "
  );
  my @ecause12 = (
  );
  my @ecause13 = (
  );
  my @ecause14 = (
  );
  
  @template = (
  "#include \"encoding.h\"\n",
  "#if __riscv_xlen == 64",
  "  #define LREG ld",
  "  #define SREG sd",
  "  #define LREGU lwu",
  "  #define REGBYTES 8",
  "  #define FMV fmv.d.x",
  "#else",
  "  #define LREG lw",
  "  #define LREGU lw",
  "  #define SREG sw",
  "  #define REGBYTES 4",
  "  #define FMV fmv.w.x",
  "#endif",
  "",
  "# User defined functions to be called",
  ".globl _test",
  "_test:",
  "    ret",
  "",
  ".align 2",
  ".globl user_trap_handler",
  "user_trap_handler:",
  "  addi sp, sp, -32*REGBYTES",
  "",
  "  SREG x1, 1*REGBYTES(sp)",
  "  SREG x2, 2*REGBYTES(sp)",
  "  SREG x3, 3*REGBYTES(sp)",
  "  SREG x4, 4*REGBYTES(sp)",
  "  SREG x5, 5*REGBYTES(sp)",
  "  SREG x6, 6*REGBYTES(sp)",
  "  SREG x7, 7*REGBYTES(sp)",
  "  SREG x8, 8*REGBYTES(sp)",
  "  SREG x9, 9*REGBYTES(sp)",
  "  SREG x10, 10*REGBYTES(sp)",
  "  SREG x11, 11*REGBYTES(sp)",
  "  SREG x12, 12*REGBYTES(sp)",
  "  SREG x13, 13*REGBYTES(sp)",
  "  SREG x14, 14*REGBYTES(sp)",
  "  SREG x15, 15*REGBYTES(sp)",
  "  SREG x16, 16*REGBYTES(sp)",
  "  SREG x17, 17*REGBYTES(sp)",
  "  SREG x18, 18*REGBYTES(sp)",
  "  SREG x19, 19*REGBYTES(sp)",
  "  SREG x20, 20*REGBYTES(sp)",
  "  SREG x21, 21*REGBYTES(sp)",
  "  SREG x22, 22*REGBYTES(sp)",
  "  SREG x23, 23*REGBYTES(sp)",
  "  SREG x24, 24*REGBYTES(sp)",
  "  SREG x25, 25*REGBYTES(sp)",
  "  SREG x26, 26*REGBYTES(sp)",
  "  SREG x27, 27*REGBYTES(sp)",
  "  SREG x28, 28*REGBYTES(sp)",
  "  SREG x29, 29*REGBYTES(sp)",
  "  SREG x30, 30*REGBYTES(sp)",
  "  SREG x31, 31*REGBYTES(sp)",
  "  csrr a0, mstatus",
  "  csrr a0, mepc",
  "  csrr a0, mtvec",
  "  csrr a0, mcause", 
  "  li a2, 0",
  "  beq a0, a2, 1f",
  "  li a2, 1",
  "  beq a0, a2, 1f",
  "  csrr a1, mepc   ",
  "  li a2, 2",
  "  beq a0, a2, inst32",
  "1:",
  "  lh a2, (a1)",
  "  # check the lower 2 bits to see if the instruction is 32-bit or 16-bit.",
  "  andi a2, a2, 0x3;",
  "  li t0, 0x3",
  "  bne a2,t0,inst16",
  "inst32:                           # is 32-bit instruction then increment by 4",
  "  addi a1,a1,0x4",
  "  beqz x0,1f",
  "inst16:",
  "  addi a1,a1,0x2                  # is 16-bit instruction then increment by 2",
  "1: ",
  "  csrw mepc, a1                   # point mepc to the next instruction.",
  "",
  "",
  "  LREG x1, 1*REGBYTES(sp)",
  "  LREG x2, 2*REGBYTES(sp)",
  "  LREG x3, 3*REGBYTES(sp)",
  "  LREG x4, 4*REGBYTES(sp)",
  "  LREG x5, 5*REGBYTES(sp)",
  "  LREG x6, 6*REGBYTES(sp)",
  "  LREG x7, 7*REGBYTES(sp)",
  "  LREG x8, 8*REGBYTES(sp)",
  "  LREG x9, 9*REGBYTES(sp)",
  "  LREG x10, 10*REGBYTES(sp)",
  "  LREG x11, 11*REGBYTES(sp)",
  "  LREG x12, 12*REGBYTES(sp)",
  "  LREG x13, 13*REGBYTES(sp)",
  "  LREG x14, 14*REGBYTES(sp)",
  "  LREG x15, 15*REGBYTES(sp)",
  "  LREG x16, 16*REGBYTES(sp)",
  "  LREG x17, 17*REGBYTES(sp)",
  "  LREG x18, 18*REGBYTES(sp)",
  "  LREG x19, 19*REGBYTES(sp)",
  "  LREG x20, 20*REGBYTES(sp)",
  "  LREG x21, 21*REGBYTES(sp)",
  "  LREG x22, 22*REGBYTES(sp)",
  "  LREG x23, 23*REGBYTES(sp)",
  "  LREG x24, 24*REGBYTES(sp)",
  "  LREG x25, 25*REGBYTES(sp)",
  "  LREG x26, 26*REGBYTES(sp)",
  "  LREG x27, 27*REGBYTES(sp)",
  "  LREG x28, 28*REGBYTES(sp)",
  "  LREG x29, 29*REGBYTES(sp)",
  "  LREG x30, 30*REGBYTES(sp)",
  "  LREG x31, 31*REGBYTES(sp)",
  "  addi sp, sp, 32*REGBYTES",
  "  mret",
  
  "# Instructions to be inserted before and after branch",
  ".macro pre_branch_macro",
  random_ecause(),
  random_ecause(),
  random_ecause(),
  random_ecause(),
  ".endm",
  "",
  ".macro post_branch_macro",
  random_ecause(),
  random_ecause(),
  random_ecause(),
  random_ecause(),
  random_ecause(),
  ".endm",
  "",
  "#------------------------------",
  "# exception generation",
  "#------------------------------",
  "",
  );
  foreach my $key (sort keys %exception_template) {
    if (($key !~ /ecause03/) && ($key !~ /ecause08/) && ($key !~ /ecause09/) && ($key !~ /ecause11/) ) {
      foreach my $count (1 .. $excepCount{$key}) {
        $exception_template{$key} .= "\n.macro $key\_$count";
        if ($key =~ /^ecause00$/) {
          $index = int(rand(scalar(@ecause00)));
          print "ecause00: $index\n";
          $exception_template{$key} .= $ecause00[$index];
        }
        elsif ($key =~ /^ecause01$/) {
          $index = int(rand(scalar(@ecause01)));
          print "ecause01: $index\n";
          $exception_template{$key} .= $ecause01[$index];
        }
        elsif ($key =~ /^ecause02$/) {
          $exception_template{$key} .= "\n\n  $illegal_instr_list[int(rand(@illegal_instr_list))]\n";
        }
        elsif ($key =~ /^ecause04$/) {
          $index = int(rand(scalar(@ecause04)));
          $exception_template{$key} .= $ecause04[$index];
        }
        elsif ($key =~ /^ecause05$/) {
          $index = int(rand(scalar(@ecause05)));
          $exception_template{$key} .= $ecause05[$index];
        }
        elsif ($key =~ /^ecause06$/) {
          $index = int(rand(scalar(@ecause06)));
          $exception_template{$key} .= $ecause06[$index];
        }
        elsif ($key =~ /^ecause07$/) {
          $index = int(rand(scalar(@ecause07)));
          $exception_template{$key} .= $ecause07[$index];
        }
        $exception_template{$key} .= "\n.endm\n";
      }
    }
    else {
      $exception_template{$key} .= "\n.macro $key";
      if ($key =~ /^ecause03$/) {
        $exception_template{$key} .= "\n\n  ebreak\n";
      }
      elsif ( ($key =~ /ecause08/) || ($key =~ /ecause09/) || ($key =~ /ecause11/) ) {
        $exception_template{$key} .= "\n\n  ecall\n";
      }
      $exception_template{$key} .= "\n.endm\n";
    }
    push @template, $exception_template{$key};
  }
  
  push @template,"# Instructions to be inserted before and after the program";
  push @template,"# Possible setups inside this macro can be";
  push @template,"#   1. Privilege mode change";
  push @template,"#   2. PMP Configuration - csrw pmpcfg0 xxx, csrw pmpcfg1 xxx"; 
  push @template, ".macro pre_program_macro";
  if ($testType =~ /v/) {
    push @template, ".global extra_boot";
    push @template, "extra_boot:";
    push @template, " ret";
	  push @template, ".globl		userstart";
	  push @template, ".type		userstart, \@function";
    push @template, "userstart:";
    push @template, "la sp, begin_signature";
    push @template, "LREG  x1, 0*REGBYTES(sp)";
    push @template, "LREG  x3, 1*REGBYTES(sp)";
    push @template, "LREG  x4, 2*REGBYTES(sp)";
    push @template, "LREG  x5, 3*REGBYTES(sp)";
    push @template, "LREG  x6, 4*REGBYTES(sp)";
    push @template, "LREG  x7, 5*REGBYTES(sp)";
    push @template, "LREG  x8, 6*REGBYTES(sp)";
    push @template, "LREG  x9, 7*REGBYTES(sp)";
    push @template, "LREG  x10,8*REGBYTES(sp)";
    push @template, "LREG  x11,9*REGBYTES(sp)";
    push @template, "LREG  x12,10*REGBYTES(sp)";
    push @template, "LREG  x13,11*REGBYTES(sp)";
    push @template, "LREG  x14,12*REGBYTES(sp)";
    push @template, "LREG  x15,13*REGBYTES(sp)";
    push @template, "LREG  x16,14*REGBYTES(sp)";
    push @template, "LREG  x17,15*REGBYTES(sp)";
    push @template, "LREG  x18,16*REGBYTES(sp)";
    push @template, "LREG  x19,17*REGBYTES(sp)";
    push @template, "LREG  x20,18*REGBYTES(sp)";
    push @template, "LREG  x21,19*REGBYTES(sp)";
    push @template, "LREG  x22,20*REGBYTES(sp)";
    push @template, "LREG  x23,21*REGBYTES(sp)";
    push @template, "LREG  x24,22*REGBYTES(sp)";
    push @template, "LREG  x25,23*REGBYTES(sp)";
    push @template, "LREG  x26,24*REGBYTES(sp)";
    push @template, "LREG  x27,25*REGBYTES(sp)";
    push @template, "LREG  x28,26*REGBYTES(sp)";
    push @template, "LREG  x29,27*REGBYTES(sp)";
    push @template, "LREG  x30,28*REGBYTES(sp)";
    push @template, "LREG  x31,29*REGBYTES(sp)";

    push @template, "#ifdef __riscv_flen";
    push @template, "fssr    x0";
    push @template, "FMV  f0, x1";
    push @template, "FMV  f1, x1";
    push @template, "FMV  f2, x2";
    push @template, "FMV  f3, x3";
    push @template, "FMV  f4, x4";
    push @template, "FMV  f5, x5";
    push @template, "FMV  f6, x6";
    push @template, "FMV  f7, x7";
    push @template, "FMV  f8, x8";
    push @template, "FMV  f9, x9";
    push @template, "FMV  f10, x10";
    push @template, "FMV  f11, x11";
    push @template, "FMV  f12, x12";
    push @template, "FMV  f13, x13";
    push @template, "FMV  f14, x14";
    push @template, "FMV  f15, x15";
    push @template, "FMV  f16, x16";
    push @template, "FMV  f17, x17";
    push @template, "FMV  f18, x18";
    push @template, "FMV  f19, x19";
    push @template, "FMV  f20, x20";
    push @template, "FMV  f21, x21";
    push @template, "FMV  f22, x22";
    push @template, "FMV  f23, x23";
    push @template, "FMV  f24, x24";
    push @template, "FMV  f25, x25";
    push @template, "FMV  f26, x26";
    push @template, "FMV  f27, x27";
    push @template, "FMV  f28, x28";
    push @template, "FMV  f29, x29";
    push @template, "FMV  f30, x30";
    push @template, "FMV  f31, x31";
    push @template, "#endif";
  }
  if ($test =~ /_user_/) {
    push @template, "la t0, user_trap_handler";
    push @template, "csrw mtvec, t0";
    if ($test !~ /_deleg_/) {
      push @template, "csrw medeleg, 0";
    }
    push @template, "# Enter user mode.";
    push @template, "la t0, 1f";
    push @template, "csrw mepc, t0";
    push @template, "li t0, MSTATUS_MPP";
    push @template, "csrc mstatus, t0";
    push @template, "li t1, (MSTATUS_MPP & -MSTATUS_MPP) * PRV_U";
    push @template, "csrs mstatus, t1";
    push @template, "mret";
    push @template, "1:";
  }
  if ($test =~ /_supervisor_/ && $testType =~ /p/) {
    push @template, "la t0, user_trap_handler";
    push @template, "csrw mtvec, t0";
    if ($test !~ /_deleg_/) {
      push @template, "csrw medeleg, 0";
    }
    push @template, "# Enter user mode.";
    push @template, "la t0, 1f";
    push @template, "csrw mepc, t0";
    push @template, "li t0, MSTATUS_MPP";
    push @template, "csrc mstatus, t0";
    push @template, "li t1, (MSTATUS_MPP & -MSTATUS_MPP) * PRV_S";
    push @template, "csrs mstatus, t1";
    push @template, "mret";
    push @template, "1:";
  }

  push @template, ".endm";
  push @template, "";
  push @template, ".macro post_program_macro";
  if ($testType =~ /v/) {
    push @template, "li a0, 1               ";
    push @template, "ecall";
  }
  else {
    push @template, "  li t5, 1";
    push @template, "  sw t5, tohost, t4";
    push @template, "  fence.i";
    if ($test =~ /_igcar_/) {
      push @template, "  li t6, 0xF4000000";
    }
    else {
      push @template, "  li t6, 0x20000";
    }
    push @template, "  la t5, begin_signature;";
    push @template, "  sw t5, 0(t6)";
    push @template, "  la t5, end_signature";
    push @template, "  sw t5, 8(t6)";
    push @template, "  sw t5, 12(t6)";
  }
  push @template, ".endm";
  push @template, "";
 
  @template = join("\n", @template);
  print TEMPLATE @template;
}
