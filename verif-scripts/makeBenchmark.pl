#!/usr/bin/perl

#-----------------------------------------------------------
# makeAapg.pl
# Generates random tests using AAPG
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use testRunConfig;
use scriptUtils;

checkSetup();
setEnvConfig();

my $simulator = getConfig("CONFIG_SIM");
my $configPath = "$shaktiHome/verification/verif-scripts/core_configs";
my $testPath =  "$workdir/aapg";


# Parse options
GetOptions(
          qw(help)            => \my $help,
          qw(clean)           => \my $clean
);

testRunConfig::setEnvConfig();
  
my @tests = `ls -d $shaktiHome/verification/workdir/benchmarks/p/*`;chomp(@tests);
foreach my $test (@tests) {
  my $name = `basename $test`;chomp($name);
  print "\n----------------------------------------------\n";
  print " $name";
  print "\n----------------------------------------------\n";
  print `cat $test/app_log`;
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}


