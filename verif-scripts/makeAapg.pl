#!/usr/bin/perl

#-----------------------------------------------------------
# makeAapg.pl
# Generates random tests using AAPG
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use testRunConfig;
use scriptUtils;

checkSetup();
setEnvConfig();

my $simulator = getConfig("CONFIG_SIM");
my $configPath = "$shaktiHome/verification/verif-scripts/core_configs";
my $testPath =  "$workdir/aapg";


# Parse options
GetOptions(
          qw(config=s)        => \my $test_config,
          qw(list_configs)    => \my $list_configs,
          qw(test_count=s)    => \my $test_count,
          qw(type=s)    => \my $test_type,
          qw(aapg_opts=s)     => \my $aapg_opts,
          qw(submit)          => \my $submit,
          qw(debug)          => \my $debug,
          qw(parallel=s)        => \my $parallel,
          qw(help)            => \my $help,
          qw(clean)           => \my $clean
);

testRunConfig::setEnvConfig();
  
my $testConfig;
my $testType;
my $testCount;
my $testParallel;

if ($parallel) {
  $testParallel = $parallel;
}
else {
  $testParallel = 0;
}
# Test suite
if ($test_config) {
  $testConfig = $test_config;
}
else {
  $testConfig = "bringup"; 
}
# Test suite
if ($test_type) {
  $testType = $test_type;
}
else {
  $testType = "p"; 
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", 1);
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

# Test count
if ($test_count) {
  $testCount = $test_count;
}
else {
  $testCount = 1;
}

if ($submit) {
  checkBins();
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}

if ($list_configs) {
  my @configs = `ls $configPath/aapg_$coreClass*.yaml |  xargs -n 2 basename -s .py`;
  doPrint("Config list at $configPath:\n");
  foreach my $cfg (@configs) {
    print "\t--config=$cfg";
  }
  exit(0);
}
doDebugPrint("aapg generation\n");
systemCmd("rm -rf $workdir/aapg");
systemCmd("mkdir -p $workdir/aapg");
chdir("$workdir/aapg");
systemCmd("aapg setup");
my @aapg_cmd = ();
if ($testConfig =~ /^all$/) {
  my @configs = `ls $configPath/aapg_$coreClass*.yaml`; chomp(@configs);
  foreach my $config (@configs) {
   my $name = `basename -s .yaml $config`; chomp($name);
    my @date = `date +%d%m%Y%s`; chomp(@date);
    $name = "$name\_$date[0]";
    push @aapg_cmd, "aapg gen --num_programs=$testCount  --config_file=$config --output_dir=$testPath/asm --asm_name=$name $aapg_opts";

  }
}
else {
  my @configs = `ls $configPath/aapg_$coreClass\_$testConfig*.yaml`; chomp(@configs);
  if (@configs == 0) {
    doPrint("ERROR: config does not exist\n");
    exit(1);
  }
  foreach my $config (@configs) {
    my $name = `basename -s .yaml $config`; chomp($name);
    my @date = `date +%d%m%Y%s`; chomp(@date);
    $name = "$name\_$date[0]";
    push @aapg_cmd, "aapg gen --num_programs=$testCount  --config_file=$config --output_dir=$testPath/asm --asm_name=$name $aapg_opts";
  }
}
while(@aapg_cmd) {
  if (($testParallel > 0) && $testParallel < scalar(@aapg_cmd)) {
    for (1 .. $testParallel) {
      my $cmd = shift(@aapg_cmd);
      my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd($cmd);
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
  }
  elsif ($testParallel > scalar(@aapg_cmd)) {
    for (1 .. @aapg_cmd) {
      my $cmd = shift(@aapg_cmd);
       my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd($cmd);
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
  }
  else {
      my $cmd = shift(@aapg_cmd);
      systemCmd($cmd);
  }
}

createTestList("$workdir/aapg.list", "$testPath/asm", "aapg", $testType);
#if ($submit) {
#  if ($debug) {
#    systemCmd("perl -I $scriptPath $scriptPath/makeRegress.pl --list=aapg.list --sub --debug --parallel=$testParallel");
#  }
#  else {
#    system("perl -I $scriptPath $scriptPath/makeRegress.pl --list=aapg.list --sub --parallel=$testParallel");
#  }
#}
