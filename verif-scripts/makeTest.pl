#!/usr/bin/perl

#-----------------------------------------------------------
# makeTest.pl
# Generates test related files
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use POSIX ":sys_wait_h";
use testRunConfig;
use scriptUtils;
use File::Compare;

checkSetup();

# Parse options
GetOptions(
          qw(test=s)        => \my $test_name,
          qw(suite=s)       => \my $test_suite,
          qw(sim=s)         => \my $test_sim,
          qw(type=s)        => \my $test_type,
          qw(mxl)           => \my $mxl_run,
          qw(igcar)         => \my $igcar_run,
          qw(new-aapg)      => \my $new,
          qw(nodebug)       => \my $no_debug,
          qw(nospike)       => \my $no_spike,
          qw(timeout=s)     => \my $timeout,
          qw(e2h_width=s)   => \my $e2h_width,
          qw(mod-spike)     => \my $mod_spike,
          qw(debug)         => \my $debug,
          qw(help)          => \my $help,
          qw(clean)         => \my $clean
);

#doDebugPrint("lav $igcar_run");
# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}

if (!$no_debug) {
  testRunConfig::setEnvConfig();
  
}
else {
  testRunConfig::setConfigValue("CONFIG_LOG",0);
}
if (defined $ENV{'CONFIG_COV'}) {
    setConfigValue("CONFIG_COV", $ENV{'CONFIG_COV'});
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

my $HYP = 0;
if (defined $ENV{'HYP'}) {
   $HYP=1
}
#my $pwd = $ENV{'PWD'};
#my $scriptLog = `basename $0 .pl`; chomp($scriptLog);
my $riscvIncludeDir = "$shaktiHome/verification/riscv-tests";
my $testPath = "$shaktiHome/verification/";
my $csmith_path = "";
my $XLEN;
my $ISA;
my $ABI;
if (defined $ENV{'CONFIG_ISA'}) {
  $ISA = lc $ENV{'CONFIG_ISA'};
}
else {
  my @isa = `grep ISA= $shaktiHome/soc_config.inc`;
  chomp(@isa);
  if($isa[0] =~ /=(.*)/) {
    $ISA = lc $1;
  }
}

if ($ISA =~ /32/) {
  $XLEN=32;
  $ABI="ilp32";
}
else {
  $XLEN=64;
  $ABI="lp64";
}

#my $workdir = "$testPath/workdir";
doDebugPrint("Generating Test Dump Directory ------------\n");

my $testName;
my $testSuite;
my $currentTestPath;
my $test;
my $simulator;
my $testType;
my $spike;
my $mxl;
my $timeout_value;
# Test name
if (!$test_name) {
  doPrint("ERROR: Undefined test name\n");
  exit(1);
}
else {
  $testName = $test_name;
}

if (!$timeout) {
  $timeout_value = "1m";
}
else {
  $timeout_value = $timeout;
}

# Test suite
if (!$test_suite) {
  doPrint("ERROR: Undefined test suite\n");
  exit(1);
}
else {
  if ($test_suite !~ /zephyr/) {
    my @test;
    if ($test_suite =~ /^rv/) {
      $testPath .= "/riscv-tests";
      doDebugPrint("\'find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /shakti-tests/) {
      $testPath .= "/shakti-tests";
      doDebugPrint("\'find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /igcar_tests/) {
      $testPath .= "/shakti-tests/igcar_tests";
      doDebugPrint("\'find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /aapg/ ) {
      $testPath .= "/workdir/aapg/";
      doDebugPrint("\'find $testPath  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /microtesk/ ) {
      $testPath .= "/microtesk/";
      doDebugPrint("\'find $testPath  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /riscv-torture/ ) {
      $testPath .= "/workdir/riscv-torture";
      doDebugPrint("\'find $testPath  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /compliance/) {
      # setup
      #my $complianceHome = "$shaktiHome/verification/riscv_compliance_fork";
      #systemCmd("cp $complianceHome/riscv-target/eclass/meta/riscv_test.h $complianceHome/riscv-test-env/p/");
      #systemCmd("cp $complianceHome/riscv-target/eclass/compliance_test.h $complianceHome/riscv-target/spike");

      #$testPath .= "/riscv_compliance_fork/riscv-test-suite";
      #$test_suite = $1;
      #doDebugPrint("\'find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print\'\n");
      #@test = `find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print`; chomp(@test);
      $testPath .= "/compliance-tests";
      doDebugPrint("\'find $testPath  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath  -type d -name  '*$test_suite' -print`; chomp(@test);
      print "@test";
    }
    elsif ($test_suite =~ /benchmarks/) {
      $testPath .= "/riscv-tests/";
      doDebugPrint("\'find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath -not \\( -path $shaktiHome/verification/workdir -prune \\)  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    elsif ($test_suite =~ /csmith/) {

      my $temp_path = `which csmith`;
      $csmith_path = `dirname $temp_path`;
      chomp($csmith_path);
      $csmith_path = "$csmith_path/..";
      $testPath .= "/workdir/csmith";
      doDebugPrint("\'find $testPath  -type d -name  '*$test_suite' -print\'\n");
      @test = `find $testPath  -type d -name  '*$test_suite' -print`; chomp(@test);
    }
    $testSuite = $test[0];
  }
  else {
    if (defined $ENV{'ZEPHYR_PATH'}) {
      $testSuite = $ENV{'ZEPHYR_PATH'};
    }
    else {
      $testSuite = "/scratch/zephyr_rtos";
    }
  }
}

# Simulator
if (!$test_sim) {
  $simulator = "bluespec";
}
elsif ($test_sim =~ /^ncverilog$/ || $test_sim =~ /^vcs$/ || $test_sim =~ /^bluespec$/){
  $simulator = $test_sim;
}
else {
  doPrint("ERROR: Invalid simulator, --sim=[bluespec|ncverilog|vcs]\n");
  exit(1);
}

# Type
if (!$test_type) {
  $testType = "p";
}
elsif ($test_type =~ /^p$/ || $test_type =~ /^v$/) {
  $testType = $test_type;
}
else {
  doPrint("ERROR: Invalid test type, --test_type=[p|v]\n");
  exit(1);
}

# trace

if ($no_spike) {
  $spike = 0;
}
else {
  $spike = 1;
}



if ($mxl_run) {
  $mxl = 1;
}
else {
  $mxl = 0;
}
checkBins();
my @test = ();
if ($testSuite =~ /csmith/ || $testSuite =~ /benchmarks/) {
  doPrint("\'find $testSuite  -name $testName.c\'\n");
  @test = `find $testSuite  -name $testName\_main.c`; chomp(@test);
  if(!@test) {
    @test = `find $testSuite  -name $testName.c`; chomp(@test);
  }
}
elsif ($testSuite =~ /aapg/) {
  systemCmd("find $testSuite/asm -name $testName.S");
  @test = `find $testSuite/asm -name $testName.S`; chomp(@test);
}
elsif ($testSuite =~ /zephyr/) {
  if (-e "$testSuite/$testName\.elf") {
    doDebugPrint("Test elf at $testSuite/$testName.elf\n");
  }
  else {
    doPrint("ERROR: Unable to find zephyr elf!\n");
    exit(1);
  }
  systemCmd("find $testSuite -name $testName.elf");
  @test = `find $testSuite -name $testName.elf`; chomp(@test);
}
else {
  systemCmd("find $testSuite -name $testName.S");
  @test = `find $testSuite -name $testName.S`; chomp(@test);
}
if (@test > 1) {
  doPrint("ERROR: Duplicate test names\n");
  exit(1);
}
else {
  $currentTestPath = `dirname $test[0]`; chomp($currentTestPath);
}
$test = $test[0];
doDebugPrint("Running test: $test\n");

my $testDir = "$workdir/$test_suite/$testType/$testName";
if (-e "$testDir") {
  systemCmd("rm -rf $testDir");
}
systemCmd("mkdir -p $testDir");
chdir($testDir);
# Creating log for each test in the directory itself
closeLog();
openLog("$testDir/$testName.log");
#chdir("$workdir/$test_suite/$testName");
# Compiling the test
if ($test !~ /\.elf/) {
  if ($testType =~ /^v$/) {
      if ($testSuite =~ /aapg/) {
        my $template_dir = "$workdir/aapg/asm";
        #my $template_dir = "$workdir/aapg/generated_templates";
        #systemCmd("perl -I $shaktiHome/verification/verif-scripts 
        #        $shaktiHome/verification/verif-scripts/makeGenerateTemplate.pl
        #        --out_dir $template_dir
        #        --name $testName\_templates 
        #        --test $test --type v");
        systemCmd("cp $template_dir/$testName\_templates.S $testDir");
        systemCmd("cp $template_dir/$testName.ld $testDir");
        systemCmd("cp $template_dir/$testName.S $testDir");
        systemCmd("cp $workdir/aapg/work/common/crt.S $testDir");
        systemCmd("cp $workdir/aapg/work/common/encoding.h $testDir");
        #systemCmd("perl -pi -e \'s/main/userstart/\' $testDir/$testName\_dup.S");
        #systemCmd("perl -pi -e \'s/include templates.S/include templates.fsadfs/\' $testDir/$testName\_dup.S");
        systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
                    -fvisibility=hidden -nostdlib -nostartfiles -DENTROPY=0x9629af2 
                    -std=gnu99 -O2 
                    -I$testDir
                    -I$riscvIncludeDir/env
                    -I$riscvIncludeDir/env/v 
                    -T$shaktiHome/verification/verif-scripts/common/aapg_v_link.ld
                    $riscvIncludeDir/env/v/entry.S 
                    $riscvIncludeDir/env/v/*.c $testDir/$testName\_dup.S -o $testName.elf");
      }
      else {
        systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
          -fvisibility=hidden -nostdlib -nostartfiles -DENTROPY=0x9629af2 -std=gnu99 -O2 
          -I$riscvIncludeDir/env/v -I$riscvIncludeDir/isa/macros/scalar 
          -T$riscvIncludeDir/env/v/link.ld $riscvIncludeDir/env/v/entry.S 
          $riscvIncludeDir/env/v/*.c $test -o $testName.elf");
      }
  }
  elsif ($testType =~ /^p$/) {
    if ($testSuite =~ /csmith/) {
      my $csmithInc = "$shaktiHome/verification/verif-scripts/common/";
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -mcmodel=medany -static -std=gnu99
                 -O2 -ffast-math -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -c 
                 -I$riscvIncludeDir/env
                 -I /tools/csmith/runtime 
                 -I $csmith_path/include
                  $riscvIncludeDir/benchmarks/common/crt.S 
                 -o crt.o");
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -mcmodel=medany -static -std=gnu99
                 -O2 -ffast-math -fno-common -fno-builtin-printf -c
                 -I /tools/csmith/runtime 
                 -I$riscvIncludeDir/env
                 -I $riscvIncludeDir/benchmarks/common
                 -I $csmith_path/include
                  $riscvIncludeDir/benchmarks/common/syscalls.c
                 -o syscalls.o");
      systemCmd("cp $shaktiHome/verification/verif-scripts/common/syscalls_shakti.c syscalls.c");
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -mcmodel=medany -static -std=gnu99
                 -O2 -ffast-math -fno-common -fno-builtin-printf -c
                 -I /tools/csmith/runtime 
                 -I$riscvIncludeDir/env
                 -I $riscvIncludeDir/benchmarks/common
                 -I $csmith_path/include
                 $shaktiHome/verification/verif-scripts/common/syscalls_shakti.c
                 -o syscalls_shakti.o");
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -w -Os -mcmodel=medany -static -std=gnu99 -O2 -ffast-math 
        -fno-common -fno-builtin-printf  -c -I /tools/csmith/runtime -I $csmith_path/include
        $test -o $testName.o"); 
      systemCmd("riscv$XLEN-unknown-elf-gcc -T$riscvIncludeDir/benchmarks/common/test.ld 
        -I /tools/csmith/runtime 
        -I $csmith_path/include 
        $testName.o syscalls.o crt.o -o $testName\_spike.elf -static -nostdlib -nostartfiles -lgcc -lm");
      systemCmd("riscv$XLEN-unknown-elf-gcc -T$riscvIncludeDir/benchmarks/common/test.ld 
        -I /tools/csmith/runtime 
        -I $csmith_path/include 
        $testName.o syscalls_shakti.o crt.o -o $testName\_shakti.elf -static -nostdlib -nostartfiles -lgcc -lm");
    }
    elsif ($testSuite =~ /peripherals/) {
      my $periInc = "$shaktiHome/verification/tests/directed/peripherals";
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI   -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf -D__ASSEMBLY__=1 -c $periInc/common/crt.S -o crt.o");
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI  -mcmodel=medany -static -std=gnu99 -fno-common -fno-builtin-printf  -c $periInc/common/syscalls.c -o syscalls.o");
      systemCmd("riscv$XLEN-unknown-elf-gcc -w -mcmodel=medany -static -std=gnu99 -fno-builtin-printf -I $periInc/i2c/ -I $periInc/qspi/ -I $periInc/dma/ -I $periInc/plic/ -I $periInc/common/ -c $periInc/smoketests/smoke.c -o smoke.o -march=$ISA -lm -lgcc");
      systemCmd("riscv$XLEN-unknown-elf-gcc -T $periInc/common/link.ld smoke.o syscalls.o crt.o -o smoke.elf -static -nostartfiles -lm -lgcc");
    }
    elsif ($testSuite =~ /aapg/) {
        my $template_dir = "$workdir/aapg/asm";
        systemCmd("cp $template_dir/$testName\_template.S $testDir");
        systemCmd("cp $template_dir/$testName.ld $testDir");
        systemCmd("cp $template_dir/$testName.S $testDir");
        systemCmd("cp $workdir/aapg/work/common/crt.S $testDir");
        systemCmd("cp $workdir/aapg/work/common/encoding.h $testDir");
        #systemCmd("perl -I $shaktiHome/verification/verif-scripts 
        #          $shaktiHome/verification/verif-scripts/makeGenerateTemplate.pl
        #          --out_dir $template_dir
        #          --name $testName\_templates 
        #          --test $test");
        #systemCmd("cp $template_dir/$testName\_templates.S $testDir/templates.S");
        systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI  -static -mcmodel=medany 
                      -fvisibility=hidden -nostdlib -nostartfiles  
                      -I $testDir
                      -T$testDir/$testName.ld
                      $testDir/crt.S $testDir/$testName.S 
                      -o $testName.elf");
    }
    elsif ($testSuite =~ /microtesk/) {
      my $dirname = `dirname $test`; chomp($dirname);
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI  -static -mcmodel=medany 
                    -fvisibility=hidden -nostdlib -nostartfiles
                    -T$dirname/shakti.ld
                    $test -o $testName.elf");
    }
    elsif ($testSuite =~ /compliance/) {

      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
        -fvisibility=hidden -nostdlib -nostartfiles -D TEST_CASE_1 
        -I$shaktiHome/verification/compliance-tests/env 
        -T$shaktiHome/verification/compliance-tests/env/link.ld 
        $test -o $testName.elf");
      
      #systemCmd("python $shaktiHome/verification/verif-scripts/compliance_scripts/generate_signature.py 
      #      $test $shaktiHome/verification/verif-scripts/compliance_scripts/conf/spike\_rv64imafdc.yaml .");
      #systemCmd("cp $testName.reference_output $testName\_spike.reference_output");
      #systemCmd("python $shaktiHome/verification/verif-scripts/compliance_scripts/generate_signature.py 
      #      $test $shaktiHome/verification/verif-scripts/compliance_scripts/conf/$coreClass\_rv64imafdc.yaml .");
      #systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
      #  -fvisibility=hidden -nostdlib -nostartfiles 
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/ 
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/p/
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-target/eclass/ 
      #  -I.
      #  -T$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/p/link.ld 
      #  $test -o $testName.elf");
      #systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
      #  -fvisibility=hidden -nostdlib -nostartfiles 
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/ 
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/p/
      #  -I$shaktiHome/verification/riscv_compliance_fork/riscv-target/spike
      #  -I.
      #  -T$shaktiHome/verification/riscv_compliance_fork/riscv-test-env/p/link.ld 
      #  $test -o $testName\_spike.elf");
      #$spike=0;
      #systemFileCmd("riscv$XLEN-unknown-elf-objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data $testName\_spike.elf", "$testName\_spike.disass");
    }
    elsif ($testSuite =~ /benchmarks/) {
      my $testDir = `dirname $test`; chomp($testDir);
      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -DPREALLOCATE=1 
                -mcmodel=medany -static -std=gnu99 -O2 -ffast-math -fno-common -fno-builtin-printf
                 -I$riscvIncludeDir/env -I$testDir/../common -I$testDir
                 $testDir/*.c $testDir/../common/syscalls.c $testDir/../common/crt.S -static -nostdlib -nostartfiles 
                 -lm -lgcc -T $testDir/../common/test.ld
                 -o $testName\_spike.elf");

      systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -DPREALLOCATE=1 
                -mcmodel=medany -static -std=gnu99 -O2 -ffast-math -fno-common -fno-builtin-printf
                 -I$riscvIncludeDir/env -I$testDir/../common -I$testDir
                 $testDir/*.c $shaktiHome/verification/verif-scripts/common/syscalls_shakti.c 
                  $shaktiHome/verification/verif-scripts/common/crt_shakti.S -static -nostdlib -nostartfiles 
                 -lm -lgcc -T $testDir/../common/test.ld
                 -o $testName\_shakti.elf");
    }
    else {
      if ($mxl == 1) {
        systemCmd("riscv$XLEN-unknown-elf-gcc -march=rv32imaf -mabi=ilp32  -static -mcmodel=medany 
          -fvisibility=hidden -nostdlib -nostartfiles -I$riscvIncludeDir/env/p 
          -I$riscvIncludeDir/isa/macros/scalar -T$riscvIncludeDir/env/p/link.ld 
          $test -o $testName.elf");
      }
      else {
        systemCmd("riscv$XLEN-unknown-elf-gcc -march=$ISA -mabi=$ABI -static -mcmodel=medany 
          -fvisibility=hidden -nostdlib -nostartfiles -I$riscvIncludeDir/env/p 
          -I$riscvIncludeDir/isa/macros/scalar -T$riscvIncludeDir/env/p/link.ld 
          $test -o $testName.elf");
      }
    }
  }
}
else {
  systemCmd("cp $test . ");
}

# Generating the disassembly
if ($test !~ /\.elf/) {
  if ($test =~ /benchmarks/ || $test =~ /csmith/) {
    systemFileCmd("riscv$XLEN-unknown-elf-objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data $testName\_spike.elf", "$testName\_spike.disass");
    systemFileCmd("riscv$XLEN-unknown-elf-objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data $testName\_shakti.elf", "$testName\_shakti.disass");
  }
  else {
    systemFileCmd("riscv$XLEN-unknown-elf-objdump --disassemble-all --disassemble-zeroes --section=.text --section=.text.startup --section=.text.init --section=.data $testName.elf", "$testName.disass");
  }
}
else {
  systemFileCmd("riscv$XLEN-unknown-elf-objdump -D $test", "$testName.disass");
}

# Generating hex

if ($XLEN==64) {
  if ($igcar_run) {
    # systemFileCmd("elf2hex  2  1048576 $testName.elf 536870912","code.mem");
    systemFileCmd("elf2hex  2  524288 $testName.elf 536870912","code.mem");
    
  }
  else {
    if ($test =~ /benchmarks/ || $test =~ /csmith/) {  
      if ($coreClass =~ /cclass/) {
        systemFileCmd("elf2hex  8  4194304 $testName\_shakti.elf 2147483648","code.mem");        
      }
      else {
        systemFileCmd("elf2hex  8  4194304 $testName\_shakti.elf 2147483648","code.mem");
      }
    }
    else {
      if ($coreClass =~ /iclass/) {
        if ($ENV{'ICLASS_31'} == 1) {
            systemFileCmd("elf2hex  16  134217728 $testName.elf 2147483648","code.mem");
        }    
        elsif ($ENV{'ICLASS_28'} == 1) {
            systemFileCmd("elf2hex  16  16777216 $testName.elf 2147483648","code.mem");
        }
        else {
          systemFileCmd("elf2hex  16  262144 $testName.elf 2147483648","code.mem");
        }
      }
      elsif ($coreClass =~ /cclass/ || $coreClass =~ /sos/) {
	      if ($HYP==1) {
	        my $to = $ENV{'TO'};
	        my $command = "elf2hex  8 33554432 $testName.elf 2147483648";
	        print("[makeTest.pl] $command > code.mem\n");
	        my @fout;
	        `elf2hex  8 33554432 $testName.elf 2147483648 > code.mem`;
	      }
	      else {
          if ($e2h_width) {
            my $elf2hex_width = int($e2h_width);
            systemFileCmd("elf2hex  8  $elf2hex_width $testName.elf 2147483648","code.mem");
          }
          else {
            systemFileCmd("elf2hex  8  4194304 $testName.elf 2147483648","code.mem");
          }
	      }
      }
      else {
        systemFileCmd("elf2hex  8  4194304 $testName.elf 2147483648","code.mem");
      }
    }
  }
  #  systemFileCmd("cut -c1-8 code.mem", "code.mem.MSB");
  #  systemFileCmd("cut -c9-16 code.mem", "code.mem.LSB");
}
else {
  if ($coreClass =~ /cclass/) {
    systemFileCmd("elf2hex  4  8388608 $testName.elf 2147483648","code.mem");
  }
  else {
    systemFileCmd("elf2hex  4  8388608 $testName.elf 2147483648","code.mem");
  }
}

if ($spike) {
  my $spike_timeout_value = '3m';
  if ($timeout =~ /(\d+)(.*)/) {
    my $time_number = $1;
    my $time_unit = $2;
    if (int($time_number/10) == 0) {
      $spike_timeout_value = '60s';
    }
    else {
      $spike_timeout_value = join('', int($1/10), $time_unit);
    }
  } 
  if ($testSuite !~ /peripherals/) {
    if ($igcar_run) {
      if ($testSuite =~ /igcar_tests/) {
	      systemCmd("timeout --foreground $spike_timeout_value spike -c --isa=$ISA  --progsize=20 -m0:0x8000,0x80000000:0x80000000,0x20000000:0xD3000000 +signature=spike_signature.txt   $testName.elf");
      }
      else {
        #systemFileCmd("spike -l --isa=$ISA $testName.elf 2>&1","$testName\_spike.trace");
        #systemCmd("timeout --foreground $spike_timeout_value spike -c --isa=$ISA +signature=spike_signature.txt  $testName.elf");
        systemCmd("timeout --foreground $spike_timeout_value spike --log-commits --log spike.dump --isa=$ISA -m0:0x8000,0x80000000:0x80000000,0x20000000:0xD3000000 +signature=spike_signature.txt  $testName.elf ")
      }
    }
    else {
      #      if ($spike) {
      #        if ($mxl == 1) {
      #          systemFileCmd("spike -l --isa=RV32\imaf +signature=spike_signature.txt $testName.elf 2>&1","$testName\_spike.trace");
      #        }
      #        else {
      #          if ($test =~ /benchmarks/) {
      #            systemFileCmd("spike -l --isa=$ISA +signature=spike_signature.txt $testName\_spike.elf 2>&1","$testName\_spike.trace");
      #          }
      #          else {
      #            systemFileCmd("spike -l --isa=$ISA +signature=spike_signature.txt $testName.elf 2>&1","$testName\_spike.trace");
      #          }
      #        }
      #      }
      if ($mxl == 1) {
        systemCmd("timeout --foreground $spike_timeout_value spike -c --isa=RV32imaf  +signature=spike_signature.txt $testName.elf");
      }
      else {
        if ($test =~ /benchmarks/) {
          systemCmd("timeout --foreground $spike_timeout_value spike -c  --isa=$ISA  +signature=spike_signature.txt $testName\_spike.elf");
        }
        else {
          if ($test =~ /csmith/) {
          if($mod_spike) {
                    systemFileCmd("timeout --foreground $timeout_value spike -c --isa=$ISA +signature=spike_signature.txt $testName\_spike.elf", "app_log_spike");
                }
                else { 
                    systemFileCmd("timeout --foreground $spike_timeout_value spike --isa=$ISA  $testName\_spike.elf", "app_log_spike");
                }
          
        }
        else {
          if ($coreClass =~ /iclass/ || $coreClass =~ /cclass/) {
	        if ($HYP==1) {
                systemCmd("timeout --foreground $spike_timeout_value spike --log-commits --log spike.dump --isa=$ISA\h +signature=spike_signature.txt $testName.elf");
	        }
	        else {
                if($mod_spike) {
                    systemCmd("timeout --foreground $spike_timeout_value spike -c  --isa=$ISA +signature=spike_signature.txt $testName.elf");
                }
                else { 
                    systemCmd("timeout --foreground $spike_timeout_value spike --log-commits --log spike.dump --isa=$ISA +signature=spike_signature.txt $testName.elf");
                }
	        }
        }
        else {
          systemCmd("timeout --foreground $spike_timeout_value spike -c  --isa=$ISA +signature=spike_signature.txt $testName.elf");
        }
      }
      }
      }
    }
  }
}

systemCmd("ln -sf $shaktiHome/bin/* .");

if (!$timeout) {
  if ($simulator =~ /^bluespec$/) {
    $timeout_value="30m";
    if ($testSuite =~ /riscv-tests/) {
      $timeout_value="5m";
    }
    elsif ($testSuite =~ /riscv-torture/) {
      $timeout_value="90m";
    }
    elsif ($testSuite =~ /zephyr/) {
      $timeout_value = "3s";
    }
  }
}
elsif ($testSuite =~ /peripherals.*smoke/ && $simulator !~ /^bluespec$/) {
  systemFileCmd("echo 53","i2c.mem");
  $timeout_value="20m";
}
if ($testSuite !~ /compliance/ || $testSuite !~ /benchmarks/ ) {
  if ($testSuite =~ /zephyr/) {
    doDebugPrint("timeout --foreground $timeout_value ./out +rtldump \n");
    `timeout --foreground $timeout_value ./out +rtldump`;
  }
  else {
    if($HYP==1) {
	my $to = $ENV{'TO'};
        my $command = "timeout --foreground $to ./out +rtldump";
	doDebugPrint("$command\n");
	`$command`;
    }
    else {
       systemCmd("timeout --foreground $timeout_value ./out +rtldump");
    }
  }
}
if ($simulator =~ /^ncverilog$/) {
  if (-d "cov_work/scope/test") {
    my $name = join("_",$test_suite, $testType, $testName);
    $name =~ s/\//\_/g ;
    systemCmd("mkdir -p $shaktiHome/bin/cov_work/scope/$name");
    systemCmd("mv cov_work/scope/test/*  $shaktiHome/bin/cov_work/scope/$name");
  }
}

my $result = "$testName.S | $test_suite | NO_RESULT";

if ($spike) {
  my $rtl_dump = "rtl.dump";
  if ($testSuite =~ /peripherals.*smoke/) {
    my @diff = `diff -w app_log $shaktiHome/verification/tests/$test_suite/app_log`;
    #print @diff;
    if (@diff) {
      `touch FAILED`;
      $result = "$testName.S | $test_suite | FAILED";
    }
    else {
      `touch PASSED`;
      $result = "$testName.S  | $test_suite | PASSED";
    }
  }
  elsif ($test_suite =~ /csmith/) {
    if (compare("app_log_spike", "app_log") == 0) {
      `touch PASSED`;
      $result = "$testName.c  | $test_suite | PASSED";
    }
    else {
      `touch FAILED`;
      $result = "$testName.c  | $test_suite | FAILED";
    }
  }
  else {
    if (!(-e "rtl.dump")) {
      `touch FAILED`;
       $result = "$testName.S | $test_suite | FAILED";
    }
    elsif (!(-e "spike.dump")) {
      `touch FAILED`;
       $result = "$testName.S | $test_suite | FAILED";
    }
    else {
      if (($testSuite =~ /aapg/ || $testSuite =~ /rv/ || $testSuite =~ /igcar_tests/ || $testSuite =~ /riscv-torture/) && ( $testType =~ /^p$/)) {
        doDebugPrint("head -n -4 rtl.dump > temp.dump\n");
        doDebugPrint("cp temp.dump rtl.dump\n");
        `head -n -4 rtl.dump > temp.dump`; 
        `mv temp.dump rtl.dump`;
        open SPIKESIGN, "spike_signature.txt"; 
        #map { (my $s = $_) =~ s/.$/x/; $s } @in;
        my @sign1 = map { (my $sign = $_) =~s/^([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})/$4\n$3\n$2\n$1/; $sign } <SPIKESIGN>;
        close SPIKESIGN;
        open SPIKESIGN, ">spike_signature.txt"; 
        print SPIKESIGN @sign1;
        close SPIKESIGN;

        #        `cat spike_signature.txt | sed 's/.\{8\}/\& /g' | awk '{print \$4 \" \" \$3 \" \" \$2 \" \" \$1}' | sed 's/ /\n/g' > spike_sign1.txt`;
        my @diff1 = `diff -w spike_signature.txt signature`;
        my @diff = `diff -w rtl.dump spike.dump`;
        if (@diff > 0) {
          `touch FAILED`;
          $result = "$testName.S | $test_suite | FAILED";
        }
        elsif (@diff1 > 0) {
          `touch FAIL_SIGN`;
          $result = "$testName.S  | $test_suite | FAIL_SIGN";
        }
        else {
         `touch PASSED`;
          $result = "$testName.S  | $test_suite | PASSED";
        }
      }
      elsif (($testSuite =~ /aapg/ || $testSuite =~ /rv/ || $testSuite =~ /igcar_tests/ || $testSuite =~ /riscv-torture/) && ( $testType =~ /^v$/)) {
        doDebugPrint("cp temp.dump rtl.dump\n");
        if ($igcar_run) {
          doDebugPrint("head -n -14 rtl.dump > temp.dump\n");
          `head -n -14 rtl.dump > temp.dump`; 
        }
        else {
          doDebugPrint("head -n -13 rtl.dump > temp.dump\n");
          `head -n -13 rtl.dump > temp.dump`; 
        }
        `mv temp.dump rtl.dump`;
        open SPIKESIGN, "spike_signature.txt"; 
        #map { (my $s = $_) =~ s/.$/x/; $s } @in;
        my @sign1 = map { (my $sign = $_) =~s/^([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})([a-zA-Z0-9]{8})/$4\n$3\n$2\n$1/; $sign } <SPIKESIGN>;
        close SPIKESIGN;
        open SPIKESIGN, ">spike_signature.txt"; 
        print SPIKESIGN @sign1;
        close SPIKESIGN;

        #        `cat spike_signature.txt | sed 's/.\{8\}/\& /g' | awk '{print \$4 \" \" \$3 \" \" \$2 \" \" \$1}' | sed 's/ /\n/g' > spike_sign1.txt`;
        my @diff1 = `diff -w spike_signature.txt signature`;
        my @diff = `diff -w rtl.dump spike.dump`;
        if (@diff > 0) {
          `touch FAILED`;
          $result = "$testName.S | $test_suite | FAILED";
        }
        elsif (@diff1 > 0) {
          `touch FAIL_SIGN`;
          $result = "$testName.S  | $test_suite | FAIL_SIGN";
        }
        else {
         `touch PASSED`;
          $result = "$testName.S  | $test_suite | PASSED";
        }

      }
      else {
        my @diff = `diff -w rtl.dump spike.dump`;
        #print @diff;
        if (@diff) {
          `touch FAILED`;
          $result = "$testName.S | $test_suite | FAILED";
        }
        else {
         `touch PASSED`;
          $result = "$testName.S  | $test_suite | PASSED";
        }
      }
    }
    #systemFileCmd("sdiff -iW rtl.dump spike.dump", "diff");
  }
}
else {
  if ($testSuite =~ /benchmarks/) {
    `touch PASSED`;
     $result = "$testName.S  | $test_suite | DONE";
  }
}
#if ($testSuite =~ /compliance/) {
#  # spike compliance
#  #systemCmd("cp $testSuite/references/$testName\.reference_output .");
#  #systemCmd("timeout --foreground $timeout_value spike -c --isa=$ISA +signature=$testName\_spike.txt
#  #          $testName.elf");
#  #systemFileCmd("cat $testName\_spike.signature.output | sed \'s/.\\{8\\}/\& /g\' | awk \'{print \$4 \" \" \$3 \" \" \$2 \" \" \$1}\' | sed \'s/ /\\n/g\'", "$testName\_spike1.signature.output");
#  #my @diff = `diff $testName\_spike1.signature.output $testName\_spike.reference_output`;
#  #if (@diff) {
#  #  $result = "$testName.S | $test_suite | spike-compliance FAILED";
#  #  #systemFileCmd("sdiff -iW signature $testName\.reference_output", "diff");
#  #}
#  #else {
#  #  $result = "$testName.S  | $test_suite | spike-compliance PASSED";
#  #}
#
#  # shakti compliance
#  systemCmd("timeout --foreground $timeout_value ./out +rtldump");
#  #my $testDir = `dirname $test`; chomp $testDir;
#  #@diff = `diff signature $testName\_spike.txt`;
#  #if (@diff) {
#  #  `touch FAILED`;
#  #  $result .= " | shakti-compliance FAILED";
#  #  #systemFileCmd("sdiff -iW signature $testName\.reference_output", "diff");
#  #}
#  #else {
#  #  `touch PASSED`;
#  #  $result .= " | shakti-compliance PASSED";
#  #}
#
#}
if ($testSuite =~ /zephyr/) {
  open APP_LOG, "$shaktiHome/verification/verif-scripts/zephyr_app_logs/$testName.log";
  my @expected = <APP_LOG>;
  chomp(@expected);
  close APP_LOG;

  open APP_LOG, "app_log";
  my @observed = <APP_LOG>;
  chomp(@observed);
  close APP_LOG;

  for (my $i=0; $i < @expected; $i++) {
    if ($expected[$i] !~ /^*/) {
      if ($expected[$i] ne $observed[$i]) {
        exit(1);
      }
    }
  }
  
  $result = "$testName | $test_suite | PASSED";
}
doDebugPrint("---------------------------------------------\n");
if ($result =~ /FAIL/) { 
  doPrint("testDir: $testDir\n");
}
else {
  doDebugPrint("testDir: $testDir\n");
  if ($result =~ /PASSED/) {
    if ($ENV{'CONFIG_COV'} == 1) {
      systemCmd("xmlcoverage gen-cov --log=$testDir");
    }
  }
}
doPrint("$result\n");
doDebugPrint("---------------------------------------------\n");
closeLog();
appendLog("$workdir/$scriptLog.log");
