#!/usr/bin/perl

#-----------------------------------------------------------
# makeTorture.pl
# Generates riscv-torture tests
#-----------------------------------------------------------

use strict;
use Getopt::Long;
use testRunConfig;
use scriptUtils;

checkSetup();
setEnvConfig();

my $simulator = getConfig("CONFIG_SIM");
my $configPath = "$shaktiHome/verification/verif-scripts/core_configs";
my $testPath =  "$workdir/riscv-torture";


# Parse options
GetOptions(
          qw(config=s)        => \my $test_config,
          qw(list_configs)    => \my $list_configs,
          qw(test_count=s)    => \my $test_count,
          qw(type=s)    => \my $test_type,
          qw(submit)          => \my $submit,
          qw(debug)          => \my $debug,
          qw(parallel=s)        => \my $parallel,
          qw(help)            => \my $help,
          qw(clean)           => \my $clean
);

testRunConfig::setEnvConfig();
  
my $testConfig;
my $testType;
my $testCount;
my $testParallel;

if ($parallel) {
  $testParallel = $parallel;
}
else {
  $testParallel = 0;
}

# Test suite
if ($test_type) {
  $testType = $test_type;
}
else {
  $testType = "p"; 
}

# Test suite
if ($test_config) {
  $testConfig = $test_config;
}
else {
  $testConfig = "bringup"; 
}

if ($debug) {
  setConfigValue("CONFIG_LOG", 1);
  $ENV{'CONFIG_LOG'} = 1;
}
else {
  if (defined $ENV{'CONFIG_LOG'}) {
    setConfigValue("CONFIG_LOG", 1);
    setConfigValue("CONFIG_LOG", $ENV{'CONFIG_LOG'});
  }
  else {
    setConfigValue("CONFIG_LOG", 0);
  }
}

# Test count
if ($test_count) {
  $testCount = $test_count;
}
else {
  $testCount = 1;
}

if ($submit) {
  checkBins();
}

# Prints command line usage of script
if ($help) {
  printHelp();
  exit(0);
}

# Clean script generated outputs
if ($clean) {
  doClean();
  exit(0);
}

if ($list_configs) {
  my @configs = `ls $configPath/torture_$coreClass*.config |  xargs -n 2 basename -s .py`;
  doPrint("Config list at $configPath:\n");
  foreach my $cfg (@configs) {
    print "\t--config=$cfg";
  }
  exit(0);
}
doDebugPrint("torture generation\n");
my @torture_cmd = ();
if ($testConfig =~ /^all$/) {
  my @configs = `ls $configPath/torture_$coreClass*.config`;
  chomp(@configs);
  foreach my $config (@configs) {
    my $testConfig = `basename $config .config`; chomp($testConfig);
    my $testGenDir = "$testPath/$testConfig";
    my $configFile = "$configPath/$testConfig\.config";
    for (my $i=0; $i < $testCount; $i++) {
      my @date = `date +%d%m%Y%s`; chomp(@date);
      my $testName = join("", "riscv-torture/",$testConfig,"_", $date[0], "_test$i");
      push @torture_cmd, "java -Xmx1G -Xss8M -XX:MaxPermSize=128M -jar sbt-launch.jar \'generator/run --config $configFile --output $testName\'";
    } 
  }
}
else {
  my @configs = `ls $configPath/torture_$coreClass\_$testConfig*.config`;
  chomp(@configs);
  foreach my $config (@configs) {
    my $testConfig = `basename $config .config`; chomp($testConfig);
    my $testGenDir = "$testPath/$testConfig";
    my $configFile = "$configPath/$testConfig\.config";
    for (my $i=0; $i < $testCount; $i++) {
      my @date = `date +%d%m%Y%s`; chomp(@date);
      my $testName = join("", "riscv-torture/",$testConfig,"_", $date[0], "_test$i");
      push @torture_cmd, "java -Xmx1G -Xss8M -XX:MaxPermSize=128M -jar sbt-launch.jar \'generator/run --config $configFile --output $testName\'";
    } 
  }
}
systemCmd("mkdir -p $workdir/riscv-torture");
chdir("$shaktiHome/verification/riscv-torture");
doDebugPrint("cd $shaktiHome/verification/riscv-torture\n");
systemCmd("rm -rf output/riscv-torture/*");
systemCmd("ln -sf $workdir/riscv-torture output/riscv-torture");
while(@torture_cmd) {
  if (($testParallel > 0) && $testParallel < scalar(@torture_cmd)) {
    for (1 .. $testParallel) {
      my $cmd = shift(@torture_cmd);
      my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd($cmd);
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
  }
  elsif ($testParallel > scalar(@torture_cmd)) {
    for (1 .. @torture_cmd) {
      my $cmd = shift(@torture_cmd);
       my $pid;
      $pid = fork();
      die if not defined $pid;
      if (not $pid) {
        my $ret = systemCmd($cmd);
        setpgrp(0,0);
        chdir('/');
        exit($ret);
      }
    }
    1 while (wait() != -1);
  }
  else {
      my $cmd = shift(@torture_cmd);
      systemCmd($cmd);
  }
}

createTestList("$workdir/riscv-torture.list", $testPath, "riscv-torture", $testType);
if ($submit) {
  if ($debug) {
    systemCmd("perl -I $scriptPath $scriptPath/makeRegress.pl --list=riscv-torture.list --sub --debug --parallel=$testParallel");
  }
  else {
    system("perl -I $scriptPath $scriptPath/makeRegress.pl --list=riscv-torture.list --sub --parallel=$testParallel");
  }
}
