#!/usr/bin/env python

import yaml

with open(".gitlab-ci.yml", 'r') as stream:
    try:
        ci_yaml = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)
fp = open("ci_script.sh", 'w') 
fp.write('#!/bin/bash\n')
for tags in ci_yaml:
    script = ci_yaml[tags]['script']
    for command in script:
        fp.write('{}\n'.format(command))
