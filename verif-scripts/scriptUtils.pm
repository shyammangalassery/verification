#!/usr/bin/perl

package scriptUtils;

use strict;
use warnings;

use Exporter qw(import);
our @EXPORT = qw(systemCmd systemFileCmd  
                 doClean doPrint doDebugPrint printHelp
                 checkSetup openLog closeLog appendLog
                 checkBins
                 $scriptLog $shaktiHome $workdir $coreClass
                 $scriptPath
                 );

our $scriptLog = `basename $0 .pl`; chomp($scriptLog);
our $shaktiHome;
our $workdir;
our $coreClass;
our $scriptPath;

sub checkSetup {
  if (defined $ENV{'SHAKTI_HOME'}) {
    $shaktiHome = $ENV{'SHAKTI_HOME'};
  }
  else {
    doPrint("ERROR: SHAKTI_HOME not defined\n");
    exit(1);
  }

  # create temporary directory where all outputs are generated
  $workdir = "$shaktiHome/verification/workdir";
  $scriptPath = "$shaktiHome/verification/verif-scripts";
  if ($shaktiHome =~ /c-class/) {
    $coreClass = "cclass";
  }
  if ($shaktiHome =~ /igcar/) {
    $coreClass = "igcar";
  }
  elsif ($shaktiHome =~ /e-class/) {
    $coreClass = "eclass";
  }
  elsif ($shaktiHome =~ /i-class/) {
    $coreClass = "iclass";
  }
  elsif ($shaktiHome =~ /sos/) {
    $coreClass = "sos"
  }

  if (!-e $workdir) {
    mkdir($workdir) or die "Can't create $workdir\n";
  }
  appendLog("$workdir/$scriptLog.log");
}

sub checkBins {
  my $out = "$shaktiHome/bin/out";
  my $boot = "$shaktiHome/bin/boot.MSB";
  my $boot1 = "$shaktiHome/bin/bootfile";

  unless (-e $out) {
    doPrint("ERROR: bin/out not present! [option: make complile_bluesim; make link_bluesim\n");
    exit(1);
  }
  
  unless ((-e $boot) or (-e $boot1)) {
    doPrint("ERROR: Boot files not present! [option: make generate_boot_files]\n");
    exit(1);
  }

}

sub openLog {
  my @file = @_;
  open LOG, ">$file[0]" or die "[$scriptLog.pl] ERROR opening file $!\n";
}

sub appendLog {
  my @file = @_;
  open LOG, ">>$file[0]" or die "[$scriptLog.pl] ERROR opening file $!\n";
}

sub closeLog {
  close LOG;
}
#-----------------------------------------------------------
# systemCmd
# Runs and displays the command line, exits on error
#-----------------------------------------------------------
sub systemCmd {
  my (@cmd) = @_;
  chomp(@cmd);
  my $cmd = join(" ", @cmd);
  $cmd =~ s/\n/   /g;
  $cmd =~ s/\s\s+/ /g;
  doDebugPrint("'$cmd'\n");
  my $ret = system("$cmd > /dev/null 2>&1");
  #my $ret = system("@cmd |& tee -a $pwd/$script.log");
  if ($ret) {
    if ($cmd =~ /^find/) { `touch FAIL_SETUP`};
    if ($cmd =~ /^riscv.*-unknown-elf-gcc/) { `touch FAIL_COMPILE`};
    if ($cmd =~ /timeout.*spike/) { `touch FAIL_MODEL`};
    if ($cmd =~ /sdiff -W rtl.dump spike.dump/) { `touch FAIL_RTL`};
    if ($cmd =~ /timeout.*out/) { `touch FAIL_RTL_TIMEOUT`};
    doPrint("ERROR: While running '$cmd'\n\n");  
    exit(1);
  }
}

#-----------------------------------------------------------
# systemCmd
# Runs and displays the command line, exits on error
#-----------------------------------------------------------
sub systemFileCmd {
  my (@cmd) = @_;
  my @sysOut;
  my $ret;

  chomp(@cmd);
  
  if ($cmd[1]) {
    doDebugPrint("'$cmd[0] > $cmd[1]'\n");
    @sysOut = `$cmd[0]`;
    $ret = $?;
  }
  else {
    doDebugPrint("'$cmd[0]'\n");
    #$ret = system("@cmd 2>> $workdir/$scriptLog.log >> $workdir/$scriptLog.log");
    $ret = system("$cmd[0] > /dev/null 2>&1");
  }
  if ($ret) {
    if ($cmd[0] =~ /^riscv.*-unknown-elf-gcc/) { `touch FAIL_COMPILE`};
    if ($cmd[0] =~ /timeout.*spike/) { `touch FAIL_MODEL`};
    if ($cmd[0] =~ /sdiff -W rtl.dump spike.dump/) { `touch FAIL_RTL`};
    if ($cmd[0] =~ /timeout.*out/) { `touch FAIL_RTL_TIMEOUT`};
    doPrint("ERROR: Running '@cmd'\n\n");  
    exit(1);
  }
  else {
    if ($cmd[1]) {
      open FILE, ">$cmd[1]";
      print FILE @sysOut;
      close FILE;
    }
  }
}

#-----------------------------------------------------------
# doClean
# Deletes generated output
#------------------------------------------------------------
sub doClean {
  if ($scriptLog =~ /^makeRegress$/) {
    doPrint("Cleaning...\n");
    systemCmd("rm -rf $shaktiHome/verification/workdir/*");
    systemCmd("rm -rf $shaktiHome/verification/riscv-torture/output/riscv-torture");
  }
  elsif ($scriptLog =~ /^makeTorture$/) {
    doPrint("Cleaning...\n");
    systemCmd("rm -rf $shaktiHome/verification/workdir/riscv-torture*");
    systemCmd("rm -rf $shaktiHome/verification/riscv-torture/output/riscv-torture");
  }
  elsif ($scriptLog =~ /^makeAapg/) {
    doPrint("Cleaning...\n");
    systemCmd("rm -rf $workdir/aapg");
    systemCmd("rm -rf $workdir/aapg.list");
  }
}

#-----------------------------------------------------------
# doPrint
# Prints message
#------------------------------------------------------------
sub doPrint {
  my @msg = @_;
  print "[$scriptLog.pl] @msg";
  print LOG "[$scriptLog.pl] @msg";
}

#-----------------------------------------------------------
# doDebugPrint
# Prints message to help debug
#------------------------------------------------------------
sub doDebugPrint {
  my @msg = @_;
  if (testRunConfig::getConfig("CONFIG_LOG")) {
    print "[$scriptLog.pl] @msg";
    print LOG "[$scriptLog.pl] @msg";
  }
}

#-----------------------------------------------------------
# printHelp
# Displays script usage
#------------------------------------------------------------
sub printHelp {
  if ($scriptLog =~ /^makeRegress$/) {
    my $usage =<<USAGE;

Description: Runs regression
Usage: perl makeRegress.pl [OPTIONS]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make regress opts="[OPTIONS]"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Options:
  --submit                      Simulates all the tests in the test list
  --compile                     Compiles RTL 
  --generate                    Generates random tests
  --final                       Displays regression result by waiting till all simulation is done
  --list=TEST_LIST              Tests in the TEST_LIST file are used for regression
                                [default] --list=riscv-tests.list
  --suite=TEST_SUITE            Specified along with --generate for a specific random generation
                                [default] --suite=all
                                --suite=[all|riscv-torture|aapg]
  --filter=FILTER_PATTERN       Filters the test list based on FILTER_PATTERN
  --result=RESULT_PATTERN       Filters the regression report based on RESULT_PATTERN
                                Eg. --res=[pass|fail|timeout|compile_fail]
  --test_count=TEST_COUNT       Generates TEST_COUNT value of random tests for each config 
                                [default] --test_count=1
  --parallel=PARALLEL_COUNT     Specified along with --submit for running tests in parallel
                                [default] --parallel=0
  --test_opts=TEST_OPTIONS	Specified along with --submit to pass arguments to running test
                                [default] --test_opts=""
  --debug			Enables verbose during regression run
  --report                      Displays regression report with percentages 
  --help                        Displays this help
  --clean                       Cleans all regression work directory
USAGE

    print $usage;
  }
  if ($scriptLog =~ /^makeTest$/) {
    my $usage =<<USAGE;

Description: Compiles test
Usage: perl makeTest.pl --test=TEST_NAME --suite=TEST_SUITE [OPTIONS]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make test opts="--test=TEST_NAME --suite=TEST_SUITE [OPTIONS]"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Options:
  --test=TEST_NAME              Test name 
  --suite=TEST_SUITE            Test Suite, directory path that uniquely identifies the test
                                Examples:
                                        rv64ui 
                                        aapg
                                        riscv-torture
  --sim=SIMULATOR               Specifies the simulator
                                [default] --sim=verilator
                                --sim=[bluespec|ncverilog|vcs|verilator]
  --type=TEST_TYPE              Specifies the test type
                                [default] --type=p
                                --type=[p|v]
  --debug                       Enables verbose during run
  --nospike			Compiles and simulates the test only on the design
  --timeout=TIMEOUT_VALUE	Simulation timeout value for termination
  --help                        Displays this help
  --clean                       Cleans all test related temp files
USAGE

    print $usage;
  }
  if ($scriptLog =~ /^makeTorture$/) {
    my $usage =<<USAGE;

Description: Generates riscv-torture test
Usage: perl makeTorture.pl [OPTIONS]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make torture opts="[OPTIONS]"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Options:
  --config=TEST_CONFIG          Config used for generation
                                [default] --config=bringup
  --list_configs                Lists available configs
  --test_count=TEST_COUNT       Generates TEST_COUNT number of tests
                                [default] --test_count=1
  --submit                      Generates and simulates generated torture tests
  --debug                       Enables verbose during run
  --parallel=PARALLEL_COUNT     PARALLEL_COUNT number of forks for test generation
				when used along with --submit the value is passed 
				for running simulation in parallel
  --help                        Displays this help
  --clean                       Cleans all torture related temp files
USAGE

    print $usage;
  }
  if ($scriptLog =~ /^makeAapg$/) {
    my $usage =<<USAGE;

Description: Generates AAPG tests
Usage: perl makeAapg.pl [OPTIONS]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
make aapg opts="[OPTIONS]"
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Options:
  --config=TEST_CONFIG          Config used for generation
                                [default] --config=bringup
  --list_configs                Lists available configs
  --test_count=TEST_COUNT       Generates TEST_COUNT number of tests
                                [default] --test_count=1
  --type=TEST_TYPE		Specifies p/v tests
                                [default] --type=p
  --aapg_opts=AAPG_ARGS		Specifies options to be passed to AAPG generator
  --submit                      Generates and simulates AAPG tests
  --debug                       Enables verbose during run
  --parallel=PARALLEL_COUNT     PARALLEL_COUNT number of forks for test generation
				when used along with --submit the value is passed 
				for running simulation in parallel
  --help                        Displays this help
  --clean                       Cleans all regression work directory
USAGE

    print $usage;
  }
}


1;
