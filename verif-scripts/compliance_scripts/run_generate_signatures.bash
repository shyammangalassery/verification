#!/usr/bin/env bash
function usage() {
  if [ -n "$1" ]; then
    echo -e "> $1\n";
  fi
  echo "Usage: $0 [-t path-to-test-codes] [-c path-to-conf-files]"
  echo "  -t, --test   Path to the test files"
  echo "  -c, --conf   Path to the conf file"
  echo "  -o, --out    Path to the output files"
  echo ""
  echo "Example: bash $0 -t test_codes/ -c RV32I.yaml -o references/"
  exit 1
}

# parse params
while [[ "$#" > 0 ]]; do case $1 in
  -c|--conf) conf_file="$2"; shift;;
  -t|--test) test_loc="$2"; shift;;
  -o|--out) out_loc="$2"; shift;;
  -h|--help) usage;;
  *) usage "Unkown parameter passed";;
esac; shift; done

# verify params
if [ -z "$conf_file" ]; then usage "Path for configuration file not set"; fi;
if [ -z "$test_loc" ]; then usage "Path for test files not set"; fi;
if [ -z "$out_loc" ]; then out_loc=signatures/; fi;

# main
for test_file in ${test_loc}/*
do
    python generate_signature.py ${test_file} ${conf_file} ${out_loc}
done
