#!/usr/bin/env python
r"""
ARGUMENTS:
1 - Test Code (*.S)
2 - Config File (*.yaml)
3 - Output Signature File (<test_code>.reference_output)

OUTPUTS:


"""

import sys
import yaml
import re

test_code = sys.argv[1]
file_conf = sys.argv[2]
dir_out = sys.argv[3]


###############################################################
##
## Function Declerations
##
###############################################################

# 
def read_config(file_name):
    config_list = yaml.load(open(file_name, 'r'))
    test_impl_file = open("implementation.h", "w")
    test_impl_file.write("# header file to be used by tests for config parameter values\n\n")
    test_impl_file.write("#ifndef RISCV_IMPL_ENCODING_H\n")
    test_impl_file.write("#define RISCV_IMPL_ENCODING_H\n\n")
    test_impl_file.write("#define CSR_USTATUS\t\t\t0x000\n")
    test_impl_file.write("#define CSR_UIE\t\t\t0x004\n")
    test_impl_file.write("#define CSR_UTVEC\t\t\t0x005\n")
    test_impl_file.write("#define CSR_UEPC\t\t\t0x041\n")
    test_impl_file.write("#define CSR_UCAUSE\t\t\t0x042\n")
    test_impl_file.write("#define CSR_UTVAL\t\t\t0x043\n")
    for k,v in config_list.items():
        v = str(config_list[k])
        if k == 'MISA_S':
            if v == '0':
                test_impl_file.write("#define SUPERVISOR\t\t\t0\n")
            else:
                test_impl_file.write("#define SUPERVISOR\t\t\t1\n")
        if k == 'MISA_U':
            if v == '0':
                test_impl_file.write("#define USER\t\t\t0\n")
            else:
                test_impl_file.write("#define USER\t\t\t1\n")
    
    test_impl_file.write("\n\n#endif\n")
    test_impl_file.close()
    return config_list
    
# 
def parse_test(file_name, config_list, fout):
    test_part_flag = False
    test_val = True
    
    with open(test_code, 'r') as fin:
        
        test_part_flag = False
        test_case_number = '0'
        line_number = 0
        test_part_skipped = 0

        for line in fin:
            line_number += 1
            line = line.strip()
            
            if line == "":
                continue
            
            if bool(re.match(r"^#", line)) == True:
                continue

            if "RVTEST_PART_START" in line:
                if test_part_flag == True:
                    print("{}:{}: Did not finish ({}) start".format(file_in, line_number, test_case_number))
                    exit()
                args = [temp.strip() for temp in (line.strip()).replace('RVTEST_PART_START','')[1:-1].split(',')]
                
                if int(test_case_number) >= int(args[0]):
                    print("{}:{}: Incorrect Nameing of Test Case after ({})".format(file_in, line_number, test_case_number))
                    exit()
                
                test_case_number = args[0]                
                test_part_flag = True
            
            
            elif "RVTEST_PART_SKIP" in line:
                if bool(re.match(r"RVTEST_PART_SKIP\((.*)[0-9](.*):(.*)\s*\)", line)) == False:
                    print("{}:{}: Incorrect Syntax in {}".format(file_in, line_number, test_case_number))
                    exit()
                
                args = re.search(r'RVTEST_PART_SKIP\(\s*(.*)\s*,\s*\"(.*):(.*)\"\)', line)

                if args.group(1) != test_case_number:
                   print("{}:{}: Wrong Test Case Numbering in ({})".format(file_in, line_number, test_case_number))
                   exit()
                
                skip_config = args.group(2)
                skip_config = re.sub(r"\s*","", skip_config) 
                if skip_config in config_list.keys():
                    config_value = args.group(3)
                    config_value = re.sub(r"\s*","", config_value) 
                    config_value = re.sub(r'\"',"", config_value)
                    value = str(config_list[skip_config])
                    if value == config_value:
                        #print("{}: {} ??  {}".format(skip_config, config_value, value[0]))
                        test_val = False
                else:
                   print("{}:{}: Wrong Skip config in test case({})".format(file_in, line_number, test_case_number))
                   exit(1)
            
            elif "RVTEST_PART_END" in line and test_part_flag == True:
                args = [temp.strip() for temp in (line.strip()).replace('RVTEST_PART_END','')[1:-1].split(',')]
                if args[0] != test_case_number:
                    print("{}:{}: Wrong Test Case Numbering in ({})".format(file_in, line_number, test_case_number))
                    exit()
                
                if test_val == False:
                    test_part_skipped = test_part_skipped + 1
                else:
                    #print(test_case_number.zfill(8))
                    fout.write(test_case_number.zfill(8) + "\n")
                    
                test_part_flag = False
                test_val = True
            
            elif "RV_COMPLIANCE_CODE_END" in line:
                while(test_part_skipped > 0):
                    fout.write("f"*8 + "\n")
                    test_part_skipped = test_part_skipped - 1

                fill_signatures = int(test_case_number) % 4
                if (fill_signatures != 0):
                    fill_signatures = 4 - fill_signatures;
                    for i in range(0, fill_signatures):
                        fout.write("0"*8 + "\n")
            

        if test_part_flag != False:
            print("{}:{}: Did not finish ({}) start".format(file_in, line_number, test_case_number))
                
###############################################################
##
## Main
##
###############################################################

file_in = test_code.split('/')[-1].split('.')[0]
file_out = dir_out + '/' + test_code.split('/')[-1].split('.')[0] + ".reference_output"
print(file_out)
with open(file_out, 'w') as fout:
    config_list = read_config(file_conf)
    #print(config_list)
    #print("\n\n\n")
    parse_test(test_code, config_list, fout)
