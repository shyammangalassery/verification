// "When V=0, the bscause register holds VS-mode’s version of the scause register. When V=1,
//  bscause holds HS-mode’s version of the scause register."


#include "riscv_test.h"
#include "test_macros.h"

RVTEST_RV64M
RVTEST_CODE_BEGIN

test_Start:
  #-------------------------------------------------------------
  # Arithmetic tests
  #-------------------------------------------------------------

//    # Addresses for test data and results
//    la      x1, test_A1_data
//    la      x2, test_A1_res

    csrr    t4,misa
    li      t3 , 1 << 7
    csrs    misa,t3              //Set bit 7 of misa as 1 to enter hypervisor 
    csrr    t3,misa
    li TESTNUM, 1
//  Enter supervisor mode.
    la t0, 1f
    csrw sepc, t0
    li t0, SSTATUS_SPP
    csrc sstatus, t0
    li t5, (SSTATUS_SPP & -SSTATUS_SPP) * PRV_S
    csrs sstatus, t5
    li      t2, 0 << 7
    csrs    0xa00, t2       // SPV is set to 0
    csrr    t1, 0xa00
    sret  
1:
//   Now in HS Mode

     li      t0,  0x900000a8    // A random address is put into the register for verification purposes
     csrs    scause, t0
     li      t1,  0x800000a8    // A random address is put into the register for verification purposes
     csrs    0x242, t1

     csrr    t5 , scause
     csrr    t6 , 0x242     // Check if this bscause(t6) in HS Mode is equal to scause in VS Mode(t3 below).

    la t0, 2f
    csrw sepc, t0
    li t0, SSTATUS_SPP
    csrc sstatus, t0
    li t5, (SSTATUS_SPP & -SSTATUS_SPP) * PRV_S
    csrs sstatus, t5
    li      t2, 1 << 7
    csrs    0xa00, t2       // SPV is set to 1
    csrr    t1, 0xa00
    sret  
2:
//  Now V=1 and we should be in VS mode.

     csrr    t3 , scause
     csrr    t4 , 0x242     // Check if this bscause(t4) in VS Mode is equal to scause in HS Mode(t5 above). 

//     # Store results
//     sw      t5, 0(x2)
//     sw      t6, 4(x2)
//     sw      t3, 8(x2)
//     sw      t4, 12(x2)


//     RVTEST_IO_ASSERT_GPR_EQ(x2, t5, 0x900000a8)
//     RVTEST_IO_ASSERT_GPR_EQ(x2, t6, 0x800000a8)
//     RVTEST_IO_ASSERT_GPR_EQ(x2, t3, 0x800000a8)
//     RVTEST_IO_ASSERT_GPR_EQ(x2, t4, 0x900000a8)

//     RVTEST_IO_WRITE_STR(x31, "# Test Pass\n");
    

  TEST_PASSFAIL

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

  TEST_DATA

RVTEST_DATA_END 
