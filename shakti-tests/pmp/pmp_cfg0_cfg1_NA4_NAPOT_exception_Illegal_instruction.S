#include "compliance_test.h"
#include "compliance_model.h"

RVTEST_ISA("RV64M")
#RMODEL_RV32M
# Test Virtual Machine (TVM) used by program.


# Test code region.
RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

    # ---------------------------------------------------------------------------------------------
    RVTEST_CASE(1,"check ISA:=regex(.*I.*); \
                        def TEST_CASE_1=True")
    RVMODEL_IO_WRITE_STR(x31, "#Test-1 \n");
    RVTEST_SIGBASE(x2, test_A1_res)
 
 
    la t1, mtvec_handler  
    csrw mtvec, t1
     

  .option norvc
noexcep:

    lw t2, (t1)

    li t1, 0x20
    csrw 0x3b0, t1
    csrr t4, 0x3b0
    RVTEST_SIGUPD(x2, t4, 0x20)

    li t1, 0xf0
    csrw 0x3b1, t1
    csrr t4, 0x3b1
    RVTEST_SIGUPD(x2, t4, 0xf0)
  

    li t1, 0x13 
    csrw 0x3a0, t1
    csrr t4,0x3a0
    RVTEST_SIGUPD(x2, t4, 0x13)

    li t1, 0x1b
    csrw 0x3a0, t1
    csrr t4,0x3a0
    RVTEST_SIGUPD(x2, t4, 0x1b)

excep: 
    li t1, 0x1b
    slli t1, t1, 2
    csrw 0x3a1, t1  //causes illigal instruction exception

test_end:
    RVMODEL_IO_WRITE_STR(x31,"# Test end\n")


  RVMODEL_HALT

RVTEST_CODE_END

.align 4

mtvec_handler:

    csrr    t5, mepc
    addi    t5, t5, 2
    csrw    mepc,t5
    # Store MCAUSE
    li      t5, 2
    csrr    t4, mcause
    bne     t5, t4,test_end
    mret


RVMODEL_DATA_BEGIN

test_A1_res:
    .fill 10, 4, -1



RVMODEL_DATA_END
