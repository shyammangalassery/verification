#include "compliance_test.h"
#include "compliance_model.h"

RVTEST_ISA("RV64I")
# Test Virtual Machine (TVM) used by program.


# Test code region.
RVTEST_CODE_BEGIN

    RVMODEL_IO_INIT
    RVMODEL_IO_ASSERT_GPR_EQ(x31, x0, 0x00000000)
    RVMODEL_IO_WRITE_STR(x31, "# Test Begin\n")

    # ---------------------------------------------------------------------------------------------
    RVTEST_CASE(1,"check ISA:=regex(.*I.*); \
                        def TEST_CASE_1=True")
    RVMODEL_IO_WRITE_STR(x31, "#Test-1 \n");
    RVTEST_SIGBASE(x2, test_A1_res)
    

    la x1, test_A1_data
    la x3, test_A2_data
   
 
    la t1, mtvec_handler  
    csrw mtvec, t1
     

  .option norvc


noexcep:

    lw t2, (t1)

    li t1, 0x20
    csrw 0x3b0, t1
    csrr t4,0x3b0
    sw t4, 0(x3)
    bne     t1, t4, test_end 
    RVTEST_SIGUPD(x2, t4, 0x3b0)



    li t1, 0x0b
    csrw 0x3a0, t1
    csrr t4,0x3a0
    sw t4,0(x1)
    bne     t1, t4, test_end
    RVTEST_SIGUPD(x2, t4, 0x0b)

    li t1, 0x20
    slli t1, t1, 2
excep: 
    li      s1 , 0x0b
    la      t2 , TEST_MEMORY
    lw      t1, 1(t2) # load address misaligned
    li      s2 , 1
    bne     s1 , s2 , test_end  # if s2 != s1 (expected s1 = 1), test fails. 
 
test_end:
    RVMODEL_IO_WRITE_STR(x31,"# Test end\n")
    
 
TEST_MEMORY:
    nop
    nop

    la      t2 , TEST_MEMORY

    RVMODEL_HALT
RVTEST_CODE_END

 # ---------------------------------------------------------------------------------------------

.align 8
mtvec_handler:
  
    # increment return address
    csrr    t1 , mepc
    addi    t1 , t1 , 4
    csrw    mepc , t1


    # Store MCAUSE
    
    csrr    t1 , mcause
    li      t0 ,CAUSE_MISALIGNED_LOAD
    bne     t1, t0, test_end
  #  beq     t1, t0, test_end

    # Reformatting mstatus to keep in machine mode
    li      t0, MSTATUS_MPP
    csrs    mstatus , t0

    li      s1 , 3
    mret



# Input data section.    
 .data 

  test_A1_data:
      .dword 0x20

  test_A2_data:
      .dword 1
RVMODEL_DATA_BEGIN

test_A1_res:
    .fill 10, 4, -1


RVMODEL_DATA_END
