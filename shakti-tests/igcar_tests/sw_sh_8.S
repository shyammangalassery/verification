####################################################
#1.32 bit R/W operation on 8 bit memory
#2.16 bit R/W operation on 8 bit memory
############################################
# include"riscv_test.h"
#include "test_macros.h"

RVTEST_RV64U
RVTEST_CODE_BEGIN

  li sp,0x00003FF8;

#Memory Test Routines are as below
#Test1: Data write with read-back compare to verify sw to big-endian memory 8 bit
	li x11,0x40000000   
	li x12,0x40000010   
	mv x13,x11

fill55:
    
  li x5, 0x12345678
	sw x5,0(x13) 
	addi x13,x13,0x4
	bge x13,x12,fill55
	mv x13,x11

test155:
  
  li TESTNUM,0x01
  lwu x14,0(x13)
  li x15,0x12345678
  bne x14,x15,fail
  addi x13,x13,0x04      
  bge x13,x12,test155

#Test1: Data write with read-back compare to verify sh to big-endian memory 8 bit
	li x11,0x40000010   
	li x12,0x40000020   
  mv x13,x11

fillaa:
    
   li x5, 0x0012
	 sh x5,0(x13)
   addi x13,x13,0x2
	 bge x13,x12,fillaa   
	 mv x13,x11

testaa:
    
    li TESTNUM,0x02
    lhu x14,0(x13)
		li x15,0x0012
		bne x14,x15,fail
		addi x13,x13,0x02
		bge x13,x12,testaa   
    
TEST_PASSFAIL

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

TEST_DATA

RVTEST_DATA_END



