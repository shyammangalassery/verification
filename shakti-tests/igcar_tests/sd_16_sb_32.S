################################
#This code tests
#1.sd/ld to 16 bit memory
#2.sb/lb to 32 bit memory
###############################################
# include"riscv_test.h"
#include "test_macros.h"
RVTEST_RV64U
RVTEST_CODE_BEGIN
	
  li sp,0x00003FF8;

#Memory Test Routines are as below
#Test1: 64 bit R/W operation on 16 bit memory
  li x11,0x30000000 
  li x12,0x30000010  
  mv x13,x11

fill55:
  li x5, 0x12345678
	sd x5,0(x13) 
	addi x13,x13,0x8
	bge x13,x12,fill55
	mv x13,x11

test155:
  li TESTNUM,0x01
	ld x14,0(x13)
	li x15,0x12345678
  bne x14,x15,fail
	addi x13,x13,0x08      
	bge x13,x12,test155

#check r/w of byte to SRAM location(32 bit memory)
  li x11,0x0000000
  mv x13,x11

fillaa:
	  
    li x5, 0x0002
   	sb x5,0(x13)
		addi x13,x13,0x1
		bge x13,x12,fillaa   
		mv x13,x11

testaa:
    
    li TESTNUM,0x02 
    lbu x14,0(x13)
		li x15,0x0002
		bne x14,x15,fail
		addi x13,x13,0x1
		bge x13,x12,testaa   

TEST_PASSFAIL

RVTEST_CODE_END

  .data
RVTEST_DATA_BEGIN

TEST_DATA

RVTEST_DATA_END


